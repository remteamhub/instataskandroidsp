package com.yourappsgeek.instatask.Services;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;

import com.google.android.material.snackbar.Snackbar;
import com.yourappsgeek.instatask.SharedPreference.SaveSharedPreference;
import com.yourappsgeek.instatask.social.AccountKitIntegrator;
import com.yourappsgeek.instatask.utils.UiUtils;

import okhttp3.MultipartBody;

public class Constants {

    public static final String BASE_URL = "";
    public static final String LOGIN_URL = "";
    public static final String LOGOUT_URL = "";
    public static final String ORDERHISTORY_URL = "";
    public static final String UPDATE_STATUS_URL = "";
    public static final String CHANNEL_ID = "my_channel_01";
    public static final String CHANNEL_NAME = "Simplified Coding Notification";
    public static final String CHANNEL_DESCRIPTION = "www.simplifiedcoding.net";

    public static boolean social=false;
    public static boolean license_img=false;
    public static Dialog dialog=null;
    public static boolean dialogFlag=true;
    public static Snackbar B;
    public static Uri imagePath;
    public static String realPath;
    public static String profile_image;
    public static String Social_images;
    public static String userName;
    public static String userEmail;
    public static String phoneNumber;
    public static String userAddress;
    public static String userStatus;
    public static String userLatLong;
    public static String userFirstName;
    public static String userLastName;
    public static int userId;
    public static int userType;
    public static String userLoginPassword;
    public static String userSigupPassword;
    public static String userPhoneNumber;
    public static String social_id;
    public static String FB_image;
    public static double user_latitude;
    public static double user_longitude;
    public static String license_image;
    public static String service_type;
    public static String service_price;
    public static int Job_Id;
    public static MultipartBody.Part part;
    public static String User_ID,Provider_ID;


    public static AccountKitIntegrator integrator;
    public static ProgressDialog progressDialog;

    /*Provider Detail*/
    public static String providerId;
    public static int providerRadius;
    public static String approval_status;
    public static String job_status;
    public static String withdraw_status;
    public static String total_earning;
    public static String weekly_earning;
    public static String overall_rating;
    public static String api;
    public static String body;
    public static String title;
    public static String mesg;



//    public static final boolean isConnected(Context context) {
//        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo netinfo = cm.getActiveNetworkInfo();
//        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
//            NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
//            NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
//            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
//                return true;
//            else return false;
//        } else
//            return false;
//    }

}
