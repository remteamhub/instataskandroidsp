package com.yourappsgeek.instatask.Services;

import android.content.Intent;

import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.yourappsgeek.instatask.Notification.PushNotification;
import com.yourappsgeek.instatask.activities.ChatActivity;
import com.yourappsgeek.instatask.activities.MainActivity;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.e("remotemessage", String.valueOf(remoteMessage.getData()));
//        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
//        Notification notification = new NotificationCompat.Builder(this)
//                .setContentTitle(remoteMessage.getData().get("title"))
//                .setContentText(remoteMessage.getData().get("body"))
//                .setLargeIcon(largeIcon)
//                .setSmallIcon(R.mipmap.ic_launcher)
//                .build();


        String body=remoteMessage.getData().get("body");
        String title=remoteMessage.getData().get("title");
        String api=remoteMessage.getData().get("api");
        String msg=remoteMessage.getData().get("msg");

        PushNotification.showNotification(getApplicationContext(),title,body);
        Log.e("notificarionData",body+" "+title+" "+api+" "+msg);

//        NotificationManagerCompat manager = NotificationManagerCompat.from(getApplicationContext());
//        manager.notify(123, notification);
        Intent in = new Intent();
        if (!api.contentEquals("message")) {
            MainActivity.check11 = true;

            in.putExtra("BODY", body);
            in.putExtra("TITLE", title);
            in.putExtra("API", api);
            in.setAction("NOW");
        }else {
            ChatActivity.check12 = true;
            in.setAction("CHAT");
        }

//sendBroadcast(in);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(in);
    }


}
