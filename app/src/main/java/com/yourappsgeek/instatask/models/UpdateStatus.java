package com.yourappsgeek.instatask.models;

public class UpdateStatus {

    public int jobid;

    public UpdateStatus(UP_Status status) {
        jobid=status.jobid;
    }

    public static class UP_Status{
        int jobid;

        public UP_Status setJobid(int jobid) {
            this.jobid = jobid;
            return UP_Status.this;
        }


        public UpdateStatus build(){
            return new UpdateStatus(UP_Status.this);
        }

    }

}
