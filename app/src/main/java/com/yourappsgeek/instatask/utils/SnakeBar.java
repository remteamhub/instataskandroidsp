package com.yourappsgeek.instatask.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;
import com.stripe.android.stripe3ds2.transaction.B;
import com.yourappsgeek.instatask.R;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class SnakeBar {


    private static Snackbar B = null;

    public static void snack(HashMap<String, View.OnClickListener> actions, int priority, String message, Activity context) {
        B = Snackbar.make(context.findViewById(android.R.id.content), message, Snackbar.LENGTH_INDEFINITE);
        if (actions != null) {
            Iterator iterator = actions.entrySet().iterator();
            B.setDuration(Snackbar.LENGTH_INDEFINITE);
            while (iterator.hasNext()) {
                Map.Entry pair = (Map.Entry) iterator.next();
                B.setAction((String) pair.getKey(), (View.OnClickListener) pair.getValue());
                iterator.remove(); // avoids a ConcurrentModificationException
            }
        }
        switch (priority) {
            case 0:
                B.getView().setBackgroundColor(Color.parseColor("#66ccff"));
                break;
            case 1:
                B.getView().setBackgroundColor(Color.parseColor("#66ccff"));
                break;
            case 2:
                B.getView().setBackgroundColor(Color.parseColor("#66ff33"));
                break;
        }
        B.show();

    }

    public static void hideSnackbar(){
        if(B !=null && B.isShown()){
            B.dismiss();
        }
    }
}
