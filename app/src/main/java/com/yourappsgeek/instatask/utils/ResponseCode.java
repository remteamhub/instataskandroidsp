package com.yourappsgeek.instatask.utils;

public enum ResponseCode
{
    // ===== POSITIVE ===== //
    SUCCESS(200),

    // ===== NEGATIVE ===== //
    UNKNOWN(-999),
    CONNECTION_TIMEOUT(-10),

    // ===== RECEIVED FROM SERVER ===== //
    GENERAL_ERROR(-1),
    SESSION_TIMEOUT(-2);

    // ============================== //

    int value;

    ResponseCode(int value)
    {
        this.value = value;
    }

    public int getValue()
    {
        return value;
    }

    public static ResponseCode getCode(int errorCode)
    {
        for (ResponseCode code : ResponseCode.values())
        {
            if (code.value == errorCode)
                return code;
        }
        throw new IllegalArgumentException("Provided error code doesn't exit");
    }
}
