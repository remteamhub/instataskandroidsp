package com.yourappsgeek.instatask.utils;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

/**
 * Contains utility methods for data layer
 *
 * @author Furqan Khan
 *
 * Author Email: furqanullah717@gmail.com
 * Created on: 16/02/2018
 */

public class DataUtils
{
    /**
     * Converts the map of parameters in a query string
     *
     * @param params Parameters
     * @return Query String
     * @throws UnsupportedEncodingException Throws an exception if any parameter key or value cannot be
     *                                      encoded in UTF-8
     */
    public static String convertToQueryString(HashMap<String, String> params) throws UnsupportedEncodingException
    {
        if (params == null)
            return "";

        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet())
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"))
                    .append("=")
                    .append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        return result.toString();
    }

    /**
     * Checks the type of exception and decides what to do for specific exception types
     *
     * @param exception Exception to process
     * @return LoginResponse
     * @throws IOException Exception to be thrown
     */
    public static <T> T processException(Exception exception) throws IOException
    {
        if (exception instanceof SocketTimeoutException)
            throw (SocketTimeoutException) exception;

        if (exception instanceof ConnectException)
            throw (ConnectException) exception;

        if (exception instanceof SocketException)
            throw (SocketException) exception;

        if (exception instanceof UnknownHostException)
            throw (UnknownHostException) exception;

        return null;
    }
}
