package com.yourappsgeek.instatask.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.google.android.material.textfield.TextInputLayout;
import com.yourappsgeek.instatask.R;
import com.yourappsgeek.instatask.widgets.SynProgressDialog;


/**
 * Contains the utility methods related to UI
 *
 * @author Furqan Khan
 *
 * Author Email: furqanullah717@gmail.com
 * Created on: 16/02/2018
 */

public class UiUtils
{
    /**
     * Hides the software keyboard
     *
     * @param context Activity Context
     * @param view    UiView that is making the request
     */
    public static void hideSoftKeyboard(Context context, View view)
    {
        hideSoftKeyboard(context, view, false);
    }

    /**
     * Hides the software keyboard and clear the focus
     *
     * @param context    Activity Context
     * @param view       UiView that is making the request
     * @param clearFocus Whether to remove focus on given field
     */
    public static void hideSoftKeyboard(Context context, View view, boolean clearFocus)
    {
        if (context == null || view == null)
            return;

        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(
                Context.INPUT_METHOD_SERVICE);

        if (inputMethodManager != null)
            inputMethodManager.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);

        if (clearFocus)
            view.clearFocus();
    }

    /**
     * Clears focus from all views in the given activity
     *
     * @param activity Activity context
     */
    public static void clearFocus(Activity activity)
    {
        if (activity != null)
        {
            View focusedView = activity.getCurrentFocus();
            if (focusedView != null)
            {
                // Clear focus
                focusedView.clearFocus();
                // Hide keyboard
                hideSoftKeyboard(activity, focusedView);
            }
        }
    }

    /**
     * Returns a themed {@link ProgressDialog} instance
     *
     * @param context Activity context
     * @return {@link ProgressDialog} instance
     */
    public static ProgressDialog getProgressDialog(Context context)
    {
        ProgressDialog pDialog = new SynProgressDialog(context, R.style.ProgressDialog);

        // Hide window background for older versions
        if (pDialog.getWindow() != null && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
        {
            pDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
        // Set common properties
        pDialog.setIndeterminate(true);
        // pDialog.setIndeterminateDrawable(ContextCompat.getDrawable(context, R.drawable.progressbar_dialog));
        pDialog.setCancelable(false);

        return pDialog;
    }

    /**
     * Returns a Builder of {@link AlertDialog} with native or custom UI as configured in
     *
     * @param context Context
     * @return {@link AlertDialog.Builder} object
     */
    public static AlertDialog.Builder getAlertDialogBuilder(Context context)
    {
        return new AlertDialog.Builder(new ContextThemeWrapper(context,
                R.style.AppTheme));
    }

    /**
     * Converts value in dp to pixels
     *
     * @param context Activity context
     * @param dp      Value in dp
     * @return Value in p
     */
    public static int convertDpToPx(Context context, float dp)
    {
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dp,
                context.getResources().getDisplayMetrics()
        ));
    }

    /**
     * Converts value in sp to pixels
     *
     * @param context Activity context
     * @param sp      Value in sp
     * @return Value in px
     */
    public static int convertSpToPx(Context context, float sp)
    {
        return Math.round(TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_SP,
                sp,
                context.getResources().getDisplayMetrics()
        ));
    }

    /**
     * Shows error in {@link TextInputLayout}
     *
     * @param inputLayout TextInputLayout
     * @param message     Error message to display
     */
    public static void showFieldError(@NonNull TextInputLayout inputLayout, CharSequence message)
    {
        inputLayout.setErrorEnabled(true);
        inputLayout.setError(message);
    }

    /**
     * Shows error in {@link View}
     *
     * @param editText EditText
     * @param message  Error message to display
     */
    public static void showFieldError(@NonNull EditText editText, CharSequence message)
    {
        editText.setError(message);
        editText.requestFocus();
    }

    /**
     * Hides error from {@link TextInputLayout}
     *
     * @param inputLayout TextInputLayout
     */
    public static void hideFieldError(@NonNull TextInputLayout inputLayout)
    {
        inputLayout.setError(null);
        inputLayout.setErrorEnabled(false);
    }

    /**
     * Hides error from {@link EditText}
     *
     * @param editText {@link EditText}
     */
    public static void hideFieldError(@NonNull EditText editText)
    {
        editText.setError(null);
    }

    /**
     * Clears all user related information from session
     */
    public static void endUserSession()
    {
    }
}
