package com.yourappsgeek.instatask.utils;

import android.content.Context;
import android.util.Patterns;
import android.widget.ImageView;
import android.graphics.Bitmap;
import androidx.annotation.Nullable;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.yourappsgeek.instatask.R;

import java.text.SimpleDateFormat;
import java.util.Locale;


public class Utils
{

    /**
     * Check if the provided string parameter is null or empty
     *
     * @param str String to check
     * @return Result
     */
    public static boolean isNullOrEmptyString(@Nullable String str)
    {
        return str == null || str.equalsIgnoreCase("null") || str.isEmpty();
    }

    /**
     * Maps the provided {@literal errorCode} to {@link ResponseCode} enum. Then returns an appropriate
     * String message against the code. If code doesn't map or no message is defined for
     * {@literal errorCode}, {@literal defaultMsg} is returned.
     *
     * @param context    Activity context
     * @param errorCode  Error code to find message against
     * @param defaultMsg Default message in case no message is found against given code
     * @return Error message
     */
    public static String getErrorMessage(Context context, int errorCode, String defaultMsg)
    {
        if (isNullOrEmptyString(defaultMsg))
        {
            defaultMsg = context.getString(R.string.error_unknown);
        }
        try
        {
            ResponseCode code = ResponseCode.getCode(errorCode);
            switch (code)
            {
                case CONNECTION_TIMEOUT:
                    return context.getString(R.string.error_timeout);
                case UNKNOWN:
                    return context.getString(R.string.error_unknown);
                default:
                    return defaultMsg;
            }
        } catch (IllegalArgumentException e)
        {
            return defaultMsg;
        }
    }

    public static boolean isValidPhone(String phone)
    {
        return Patterns.PHONE.matcher(phone).matches();
    }

    public static boolean isValidEmail(String phone)
    {
        return Patterns.EMAIL_ADDRESS.matcher(phone).matches();
    }


    /**
     * Gets timestamp in millis and converts it to HH:mm (e.g. 16:44).
     */
    public static String formatTime(long timeInMillis) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        return dateFormat.format(timeInMillis);
    }

    /**
     * Crops image into a circle that fits within the ImageView.
     */
//    public static void displayRoundImageFromUrl(final Context context, final String url, final ImageView imageView) {
//        Glide.with(context)
//                .load(url)
//                .asBitmap()
//                .centerCrop()
//                .dontAnimate()
//                .into(new BitmapImageViewTarget(imageView) {
//                    @Override
//                    protected void setResource(Bitmap resource) {
//                        RoundedBitmapDrawable circularBitmapDrawable =
//                                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
//                        circularBitmapDrawable.setCircular(true);
//                        imageView.setImageDrawable(circularBitmapDrawable);
//                    }
//                });
//    }


}
