package com.yourappsgeek.instatask.utils;

/**
 * @author Furqan Khan
 *
 * Author Email: furqanullah717@gmail.com
 * Created on: 16/02/2018
 */

public final class HeaderParams
{
    public static final String AUTH_TOKEN = "authorization";
    public static final String USER_AGENT = "TrackingUser-Agent";
    public static final String CONTENT_TYPE = "accept";
    public static final String CONTENT_TYPE_DEFAULT = "application/json";
}
