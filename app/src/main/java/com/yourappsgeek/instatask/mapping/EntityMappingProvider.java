package com.yourappsgeek.instatask.mapping;


import com.yourappsgeek.instatask.config.NetworkConfig;
import com.yourappsgeek.instatask.config.NetworkResponseType;

/**
 * Provides instance of {@link EntityMapper} implementation according to NETWORK_RESPONSE_TYPE
 *
 * @author Furqan Khan
 *  ™
 * Author Email: furqanullah717@gmail.com
 * Created on: 16/02/2018
 */

public class EntityMappingProvider
{
    public static EntityMapper get() throws ParsingException
    {
        if (NetworkConfig.NETWORK_RESPONSE_TYPE == NetworkResponseType.JSON)
        {
            return new JsonEntityMapper();
        } else
        {
            throw new ParsingException("Unsupported response parser for configured network client");
        }
    }
}
