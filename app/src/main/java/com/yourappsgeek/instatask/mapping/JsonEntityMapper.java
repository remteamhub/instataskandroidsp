package com.yourappsgeek.instatask.mapping;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 * @author Furqan Khan
 *  ™
 * Author Email: furqanullah717@gmail.com
 * Created on: 16/02/2018
 */

public class JsonEntityMapper implements EntityMapper
{
    @Override
    public String transformToParsableData(Object model)
    {
        return new Gson().toJson(model);
    }

    @Override
    public <M> M transformToObject(String data, Class<M> objectClass) throws ParsingException
    {
        try
        {
            return new Gson().fromJson(data, objectClass);

        } catch (JsonSyntaxException e)
        {
            throw new ParsingException(e.getMessage(), e.getCause());
        }
    }
}
