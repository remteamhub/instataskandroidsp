package com.yourappsgeek.instatask.mapping;


/**
 * Maps model/request/response objects to configured parser format. Similarly, it creates model objects
 * from parsable data
 *
 * @author Furqan Khan
 *  ™
 * Author Email: furqanullah717@gmail.com
 * Created on: 16/02/2018
 */

public interface EntityMapper
{
    String transformToParsableData(Object model);

    <M> M transformToObject(String data, Class<M> objectClass) throws ParsingException;
}
