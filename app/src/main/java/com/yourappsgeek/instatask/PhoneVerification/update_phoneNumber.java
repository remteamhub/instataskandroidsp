package com.yourappsgeek.instatask.PhoneVerification;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.facebook.CallbackManager;
import com.yourappsgeek.instatask.ApiServices.APIService;
import com.yourappsgeek.instatask.RetrofitClient.ApiClient;
import com.yourappsgeek.instatask.Services.Constants;
import com.yourappsgeek.instatask.SharedPreference.SaveSharedPreference;
import com.yourappsgeek.instatask.activities.LicenseUpload;
import com.yourappsgeek.instatask.activities.MainActivity;
import com.yourappsgeek.instatask.social.AccountKitIntegrator;
import com.yourappsgeek.instatask.social.SocialCallback;
import com.yourappsgeek.instatask.utils.UiUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

import static com.yourappsgeek.instatask.Services.Constants.integrator;

public class update_phoneNumber extends AppCompatActivity {


    Activity activity;
    CallbackManager callbackManager;
    AccountKitIntegrator integrator;


    public update_phoneNumber(Activity activity) {

        this.activity = activity;
        callbackManager = CallbackManager.Factory.create();

    }




    public static void Phone_Verification(final Activity activity, final int user_id, final String fcm_token) {

        AccountKitIntegrator integrator = new AccountKitIntegrator(activity, new SocialCallback() {

            @Override
            public void onSocialLoginSuccess(String username, String first_name, String last_name, String email, String SocialId, String image, String userId, String accessToken, int type) {
                Log.e("phonenumber", userId);

                Log.e("userDetail", userId);
                hideProgress(activity);

                RequestBody FCM_TOKEN = RequestBody.create(MediaType.parse("text/plain"), fcm_token);
                RequestBody phone_no = RequestBody.create(MediaType.parse("text/plain"), userId);
                //UpdateProfile(activity,user_id,FCM_TOKEN,4,phone_no,null);
            }

            @Override
            public void onSocialLoginFailed(String error) {
                hideProgress(activity);
                Toast.makeText(activity, error, Toast.LENGTH_SHORT).show();
            }
        });
        integrator.phoneLogin();
    }



    static void hideProgress(Activity activity) {
        Constants.progressDialog = UiUtils.getProgressDialog(activity);
        if (Constants.progressDialog != null && Constants.progressDialog.isShowing())
            Constants.progressDialog.dismiss();
    }

        @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (integrator != null)
            integrator.onActivityResult(requestCode, data);
//        if (twitterLogin != null) twitterLogin.onActivityResult(requestCode, resultCode, data);
        if (this.callbackManager != null)
            this.callbackManager.onActivityResult(requestCode, resultCode, data);

    }
}
