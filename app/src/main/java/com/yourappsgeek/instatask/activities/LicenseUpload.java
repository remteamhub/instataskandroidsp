package com.yourappsgeek.instatask.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.MediaStore;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.yourappsgeek.instatask.Controller.ScheduleRequest;
import com.yourappsgeek.instatask.Controller.internetCheck;
import com.yourappsgeek.instatask.Services.Constants;
import com.yourappsgeek.instatask.R;
import com.yourappsgeek.instatask.ApiServices.APIService;
import com.yourappsgeek.instatask.RetrofitClient.ApiClient;
import com.yourappsgeek.instatask.SharedPreference.SaveSharedPreference;
import com.yourappsgeek.instatask.config.ImageUri;
import com.yourappsgeek.instatask.social.AccountKitIntegrator;
import com.yourappsgeek.instatask.social.SocialCallback;
import com.yourappsgeek.instatask.utils.UiUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static com.yourappsgeek.instatask.Controller.internetCheck.isConnected;
import static com.yourappsgeek.instatask.Services.Constants.realPath;
import static com.yourappsgeek.instatask.activities.ProfileActivity.PICK_IMAGE;
import static com.yourappsgeek.instatask.config.ImageUri.getImageUri;
import static com.yourappsgeek.instatask.config.ImageUri.getRealPathFromURI;

public class LicenseUpload extends AppCompatActivity {

    Button licenseUpload, licenseSubmit;
    ImageView licenseImage;
    TextView successStatus, back_to_upload;
    ProgressDialog progressDialog;
    String full_name, first_name, last_name, email, radius, password, phone, fcm_token, fb_image, Sooial_id;
    double latitude = 0.0, lontitude = 0.0;
    int user_id;
    String phone1;
    AccountKitIntegrator integrator;
    boolean social;

    private int PICK_IMAGE_REQUEST = 1;
    private static final int REQUEST_CODE = 12345;
    SharedPreferences preferences;
    Integer REQUEST_CAMERA = 1;
    private ListAdapter adapter;
    private static final String IMAGE_DIRECTORY = "/YourDirectName";
    private Context mContext;
    private int GALLERY = 1, CAMERA = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fcm_token = SaveSharedPreference.getFCM_Token(getApplicationContext());
        getSupportActionBar().hide();
        progressDialog = UiUtils.getProgressDialog(LicenseUpload.this);
        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        full_name = Constants.userName;
        first_name = Constants.userFirstName;
        last_name = Constants.userLastName;
        password = Constants.userSigupPassword;
        email = Constants.userEmail;
        phone = Constants.userPhoneNumber;
        fb_image = Constants.FB_image;
        latitude = Constants.user_latitude;
        lontitude = Constants.user_longitude;
        Sooial_id = Constants.social_id;

        social = Constants.social;

        Log.e("token", fcm_token);

        setContentView(R.layout.activity_license_upload);
        licenseUpload = findViewById(R.id.license_image_upload_button);
        licenseImage = findViewById(R.id.license_imageView);
        successStatus = findViewById(R.id.success_status_textView);
        licenseSubmit = findViewById(R.id.Submit_license_button);
        back_to_upload = findViewById(R.id.back_upload);
        back_to_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LicenseUpload.this, CustomerSignUpActivity.class));
                finish();
            }
        });

        licenseUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showFileChooser();
                 selectImage();
            }
        });
        licenseSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!isConnected(LicenseUpload.this));

                else {
                    upload();
                }
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE) {
            if (grantResults.length == 4 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED && grantResults[3] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(LicenseUpload.this, "Permission granted!!!", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(LicenseUpload.this, "Necessary permissions not granted...", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

            if (resultCode == this.RESULT_CANCELED) {
                return;
            }
            if (requestCode == GALLERY) {
                if (data != null) {
                    Uri contentURI = data.getData();
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                       // String path = saveImage(bitmap);
                      //  Toast.makeText(getApplicationContext(), "Image Saved!", Toast.LENGTH_SHORT).show();
                        licenseImage.setImageBitmap(bitmap);
                        Constants.imagePath = getImageUri(getApplicationContext(), bitmap);
//
                        successStatus.setText("Successfully uploaded");
                        realPath = getRealPathFromURI(getApplicationContext(),Constants.imagePath);
                       // circleImageView.setImageBitmap(bitmap);

                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Failed!", Toast.LENGTH_SHORT).show();
                    }
                }

            } else if (requestCode == CAMERA) {
                Bitmap thumbnail = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");

//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                // String path = saveImage(bitmap);
               // Toast.makeText(getApplicationContext(), "Image Saved!", Toast.LENGTH_SHORT).show();
                licenseImage.setImageBitmap(thumbnail);
                Constants.imagePath = getImageUri(getApplicationContext(), thumbnail);
                successStatus.setText("Successfully uploaded");
                realPath = getRealPathFromURI(getApplicationContext(),Constants.imagePath);
//                circleImageView.setImageBitmap(thumbnail);
//                saveImage(thumbnail);
//                Toast.makeText(getApplicationContext(), "Image Saved!", Toast.LENGTH_SHORT).show();
            }


       // }

    }

    private void upload() {
        try {
            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!successStatus.getText().equals("Successfully uploaded")) {
            Toast.makeText(this, "Please Select License_Image", Toast.LENGTH_SHORT).show();
            hideProgress();
        } else {

            if (realPath != null) {
                File file = new File(realPath);
                // Create a request body with file and image media type
                RequestBody fileReqBody = null;
                if (file != null) {
                    fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
                }
                // Create MultipartBody.Part using file request-body,file name and part name
                MultipartBody.Part part = MultipartBody.Part.createFormData("license_img", file.getName(), fileReqBody);

                RequestBody full_name1 = null;
                if (full_name != null) {
                    full_name1 = RequestBody.create(MediaType.parse("text/plain"), full_name);
                }
                RequestBody first_name1 = null;
                if (first_name != null) {
                    first_name1 = RequestBody.create(MediaType.parse("text/plain"), first_name);
                }

                RequestBody last_name1 = null;
                if (last_name != null) {
                    last_name1 = RequestBody.create(MediaType.parse("text/plain"), last_name);
                }
                RequestBody email1 = null;
                if (email != null) {
                    email1 = RequestBody.create(MediaType.parse("text/plain"), email);
                }
                RequestBody phone1 = null;
                RequestBody password1 = null;
                if (phone != null) {
                    phone1 = RequestBody.create(MediaType.parse("text/plain"), phone);
                }
                if (password != null) {
                    password1 = RequestBody.create(MediaType.parse("text/plain"), password);
                }

                RequestBody fcm_token1 = RequestBody.create(MediaType.parse("text/plain"), fcm_token);
                RequestBody radius1 = RequestBody.create(MediaType.parse("text/plain"), "4");
                RequestBody fb_image1 = null;
                if (fb_image != null) {
                    fb_image1 = RequestBody.create(MediaType.parse("text/plain"), fb_image);
                }
                RequestBody Sooial_id1 = null;
                if (Sooial_id != null) {
                    Sooial_id1 = RequestBody.create(MediaType.parse("text/plain"), Sooial_id);
                }


                if (part != null) {

                    if(!isConnected(LicenseUpload.this));
                    else {
                        ScheduleRequest.LicenseImageUpload(this,user_id,fcm_token1,part);
//                        if (social) {
////                            SocialLogin(Sooial_id1, full_name1, fcm_token1, email1, first_name1, last_name1, fb_image1, latitude, lontitude, radius1, part);
//                        } else {
//                            Register(full_name1, first_name1, last_name1, email1, phone1, radius1, password1, fcm_token1, part);
//                        }
                    }

                } else {
                    Toast.makeText(this, "Please add license image!", Toast.LENGTH_SHORT).show();
                }
            }
        }

    }


    private void selectImage() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {"Select photo from gallery", "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }
    public void choosePhotoFromGallary() {
//        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        startActivityForResult(galleryIntent, GALLERY);

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);

    }










//    public Uri getImageUri(Context inContext, Bitmap inImage) {
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//
//        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
//        Uri uri=null;
//        if (path!=null){
//            uri=Uri.parse(path);
//        }
//        return uri;
//    }
//
//    public String getRealPathFromURI(Uri uri) {
//        if (uri == null) {
//            return null;
//        }
//        Cursor cursor = null;
//        String result = null;
//        int column_index = 0;
//        try {
//            String[] projection = {MediaStore.Images.Media.DATA};
//            cursor = getContentResolver().query(uri, projection, null, null, null);
//
//            if (cursor != null) {
//                column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//                cursor.moveToFirst();
//                result=cursor.getString(column_index);
//            }
//
//        }catch (Exception e){
//
//            e.printStackTrace();
//        }finally {
//            try {
//                if (cursor != null && !cursor.isClosed()) {
//                    cursor.close();
//                }
//
//            } catch (Exception e) {
//                Log.e("While closing cursor", String.valueOf(e));
//            }
//        }
//
//
//
//        return result;
//    }

    void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }



}
