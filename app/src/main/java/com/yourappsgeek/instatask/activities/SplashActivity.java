package com.yourappsgeek.instatask.activities;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.yourappsgeek.instatask.Controller.internetCheck;
import com.yourappsgeek.instatask.R;
import com.yourappsgeek.instatask.Services.Constants;
import com.yourappsgeek.instatask.SharedPreference.SaveSharedPreference;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {
    NotificationManager nManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if((getIntent().getFlags() & Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY) != 0){

        }else {
            if (getIntent().getExtras() != null) {
                String api = getIntent().getExtras().getString("api");
                String body = getIntent().getExtras().getString("body");
                String title = getIntent().getExtras().getString("title");
                Log.e("mesg", api+" "+title+" "+body);
                Log.e("notificarionData",api+" "+body);
                Constants.mesg = body;
                if (api != null && body != null && title != null) {
                    Constants.api = api;
                    Constants.body = body;
                    Constants.title = title;

                    getIntent().removeExtra("api");
                    getIntent().removeExtra("body");
                    getIntent().removeExtra("title");
                    getIntent().setData(null);

                }
            }
        }
        nManager = ((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE));





//        if (getIntent().getExtras() != null) {
//            Log.e("extras", String.valueOf(getIntent().getExtras()));
//
//        }

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.yourappsgeek.instatask",
                    //  Package name
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
        } catch (NoSuchAlgorithmException e) {
        }
        TimerTask navigateTask = new TimerTask() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                            if (SaveSharedPreference.getLoggedStatus(getApplicationContext())) {
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);

                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finishAffinity();
                                finish();
                            } else {
                                gotoLoginScreen();
                            }

                    }
                });

            }
        };
        new Timer().schedule(navigateTask, 1200);




    }

    private void gotoLoginScreen() {
        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
        finish();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        nManager.cancelAll();
        try{
           deleteCache(getApplicationContext());
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) { e.printStackTrace();}
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }




}
