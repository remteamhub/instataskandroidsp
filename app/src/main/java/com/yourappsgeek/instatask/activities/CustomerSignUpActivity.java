package com.yourappsgeek.instatask.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;
import com.google.android.material.textfield.TextInputLayout;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.yourappsgeek.instatask.Controller.ScheduleRequest;
import com.yourappsgeek.instatask.Controller.internetCheck;
import com.yourappsgeek.instatask.Interfaces.ScheduleJobCallbackListener;
import com.yourappsgeek.instatask.Services.Constants;
import com.yourappsgeek.instatask.R;
import com.yourappsgeek.instatask.ApiServices.APIService;
import com.yourappsgeek.instatask.RetrofitClient.ApiClient;
import com.yourappsgeek.instatask.SharedPreference.SaveSharedPreference;
import com.yourappsgeek.instatask.models.HistoryItem;
import com.yourappsgeek.instatask.models.UpdateStatus;
import com.yourappsgeek.instatask.social.AccountKitIntegrator;
import com.yourappsgeek.instatask.social.FacebookIntegrator;
import com.yourappsgeek.instatask.social.SocialCallback;
import com.yourappsgeek.instatask.social.TwitterIntegrator;
import com.yourappsgeek.instatask.utils.UiUtils;
import com.yourappsgeek.instatask.utils.Utils;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.yourappsgeek.instatask.Controller.internetCheck.isConnected;

public class CustomerSignUpActivity extends AppCompatActivity implements View.OnClickListener, ScheduleJobCallbackListener {
    AccountKitIntegrator integrator;
    FacebookIntegrator facebookIntegrator;
    CallbackManager callbackManager;
    TwitterIntegrator twitterIntegrator;
    SharedPreferences.Editor editor;
    String license=null;

    @BindView(R.id.twitterLogin)
    TwitterLoginButton twitterLogin;

    private ProgressDialog progressDialog;
    @BindView(R.id.edtFullName)
    EditText edtFullName;

    @BindView(R.id.edtEmail)
    EditText edtEmail;

    @BindView(R.id.edtPassword)
    EditText edtPassword;

    @BindView(R.id.edtConfirmPassword)
    EditText edtConfirmPassword;

    @BindView(R.id.wrapperFullName)
    TextInputLayout wrapperFullName;

    @BindView(R.id.wrapperEmail)
    TextInputLayout wrapperEmail;

    @BindView(R.id.wrapperPassword)
    TextInputLayout wrapperPassword;

    @BindView(R.id.wrapperConfirmPassword)
    TextInputLayout wrapperConfirmPassword;

    @BindView(R.id.cbTerms)
    CheckBox cbTerms;

    @BindView(R.id.fblogin)
    LoginButton fblogin;

    SharedPreferences preferences;

    String fcm_token;


    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_signup);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        ButterKnife.bind(this);
        progressDialog = UiUtils.getProgressDialog(CustomerSignUpActivity.this);

        //add click listeners
        callbackManager = CallbackManager.Factory.create();

        fcm_token =SaveSharedPreference.getFCM_Token(getApplicationContext());
        Log.e("token", fcm_token);
        editor = getSharedPreferences("SocialLogout", MODE_APPEND).edit();
        editor.apply();


        findViewById(R.id.btnSignUp).setOnClickListener(this);
        findViewById(R.id.btnFacebook).setOnClickListener(this);
        findViewById(R.id.btnTwitter).setOnClickListener(this);
        findViewById(R.id.txtAlreadyHaveAccount).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSignUp: {


                if(!isConnected(CustomerSignUpActivity.this));
                else {
                    doSignUp();
                }
                break;
            }
            case R.id.btnFacebook: {
                if(!isConnected(CustomerSignUpActivity.this));

                else {
                    doFbLogin();
                }
                break;
            }
            case R.id.btnTwitter: {
                if(!isConnected(CustomerSignUpActivity.this));

                else {
                    doTwitterLogin();
                }
                break;
            }
            case R.id.txtAlreadyHaveAccount: {
                startActivity(new Intent(CustomerSignUpActivity.this, LoginActivity.class));
                finish();
                break;
            }
        }
    }

    public void doTwitterLogin() {
        twitterIntegrator = new TwitterIntegrator(twitterLogin, new SocialCallback() {
            @Override
            public void onSocialLoginSuccess(String username, String first_name, String last_name, String email, String SocialId, String image, String userId, String accessToken, int type) {

                setSocialUser(SocialId, username,  email, first_name, last_name, image);

            }

            @Override
            public void onSocialLoginFailed(String error) {
                Toast.makeText(CustomerSignUpActivity.this, "Social login failed", Toast.LENGTH_SHORT).show();
            }
        });
        twitterIntegrator.login();
    }

    public void doFbLogin() {
        facebookIntegrator = new FacebookIntegrator(fblogin, callbackManager, new SocialCallback() {
            @Override
            public void onSocialLoginSuccess(String username, String SocialId, String email, String first_name, String last_name, String image, String userId, String accessToken, int type) {
                progressDialog.show();
                setSocialUser(SocialId, username,  email, first_name, last_name, image);
            }

            @Override
            public void onSocialLoginFailed(String error) {
                Toast.makeText(CustomerSignUpActivity.this, error, Toast.LENGTH_SHORT).show();
            }
        });
        facebookIntegrator.login();
    }

    public void doSignUp() {
        final String email;
        final String password;
        String confirmPass;
        String fullname;
        final String first_name;
        final String last_name;

        email = edtEmail.getText().toString();
        password = edtPassword.getText().toString();
        confirmPass = edtConfirmPassword.getText().toString();
        fullname = edtFullName.getText().toString();
        final int random = new Random().nextInt(1000000);
        int firstSpace = fullname.indexOf(" ");
        if (firstSpace < 0) {
            wrapperFullName.setErrorEnabled(true);
            wrapperFullName.setError("Please Enter first name");
            return;
        } else {
            first_name = fullname.substring(0, firstSpace);
            last_name = fullname.substring(firstSpace).trim();
            if (last_name.equals("")) {
                wrapperFullName.setErrorEnabled(true);
                wrapperFullName.setError("Please Enter last name");
                return;
            } else if (first_name.equals(last_name)) {
                wrapperFullName.setErrorEnabled(true);
                wrapperFullName.setError("Please Correct your name");
                return;
            }
        }
        fullname = first_name + last_name + random;

        wrapperPassword.setErrorEnabled(false);
        wrapperEmail.setErrorEnabled(false);
        wrapperFullName.setErrorEnabled(false);
        wrapperConfirmPassword.setErrorEnabled(false);
        if (fullname.isEmpty()) {
            wrapperFullName.setErrorEnabled(true);
            wrapperFullName.setError("Can't be empty");
            return;
        }
        if (!Utils.isValidEmail(email)) {
            wrapperEmail.setErrorEnabled(true);
            wrapperEmail.setError("Invalid Email");
            return;
        }
        if (password.length() < 8) {
            wrapperPassword.setErrorEnabled(true);
            wrapperPassword.setError("Password length must be greater than 6");
            return;
        }
        if (!confirmPass.equals(password)) {
            wrapperConfirmPassword.setErrorEnabled(true);
            wrapperConfirmPassword.setError("Password does not match.");
            return;
        }


        if (!cbTerms.isChecked()) {
            Toast.makeText(this, "Please accept terms and policies", Toast.LENGTH_SHORT).show();
            return;
        }

        final String finalFullname = fullname;
        final String Email = email;
        integrator = new AccountKitIntegrator(this, new SocialCallback() {
            @Override
            public void onSocialLoginSuccess(String username, String f_name, String l_name, String email, String SocialId, String image, String userId, String accessToken, int type) {
                progressDialog = UiUtils.getProgressDialog(CustomerSignUpActivity.this);
                progressDialog.setMessage("Wait for Verification...");
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.show();
                setUser(finalFullname, first_name, last_name, Email, "4", password, userId, fcm_token);
            }

            @Override
            public void onSocialLoginFailed(String error) {
                progressDialog = UiUtils.getProgressDialog(CustomerSignUpActivity.this);
                progressDialog.dismiss();
                Toast.makeText(CustomerSignUpActivity.this, "Phone verification failed", Toast.LENGTH_SHORT).show();
            }
        });
        integrator.phoneLogin();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (integrator != null)
            integrator.onActivityResult(requestCode, data);
        if (twitterLogin != null) twitterLogin.onActivityResult(requestCode, resultCode, data);
        if (this.callbackManager != null)
            this.callbackManager.onActivityResult(requestCode, resultCode, data);

    }

    void setUser(String userName, String fName, String lName, String email, String radius, final String password, String phone, final String token) {
        hideProgress();
        Constants.social=false;

        Constants.userName = userName;
        Constants.userFirstName = fName;
        Constants.userLastName = lName;
        Constants.userEmail = email;
        Constants.userSigupPassword = password;
        Constants.userPhoneNumber = phone;

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("fulName", userName);
        editor.putString("first_name", fName);
        editor.putString("last_name", lName);
        editor.putString("loginPassword", password);
        editor.putString("email", email);
        editor.putString("radius", radius);
        editor.putString("phone", phone);
        editor.putString("name",fName+" "+lName);
        editor.apply();

        ScheduleRequest scheduleRequest=new ScheduleRequest(getApplicationContext(),CustomerSignUpActivity.this,CustomerSignUpActivity.this, getParent());
        scheduleRequest.Register(getApplicationContext(),userName,fName,lName,email,phone,4,password,fcm_token);



    }



    void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    void setSocialUser(String social_id, final String username, String email, final String first_name, final String last_name, String image) {
        final APIService request = ApiClient.getClient().create(APIService.class);
        //Defining the user object as we need to pass it with the call
        Constants.social_id = social_id;
        Constants.userName = username;
        Constants.userEmail = email;
        Constants.userFirstName = first_name;
        Constants.userLastName = last_name;
        //Constants.profile_image = image;


        Constants.social=true;

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("fulName", username);
        editor.putString("first_name", first_name);
        editor.putString("last_name", last_name);
        editor.putString("email", email);
        double latitude=Constants.user_latitude;
        double longitude=Constants.user_longitude;
        editor.putString("name",first_name+" "+last_name);

        editor.apply();

        fcm_token=SaveSharedPreference.getFCM_Token(getApplicationContext());
        ScheduleRequest scheduleRequest=new ScheduleRequest(getApplicationContext(),CustomerSignUpActivity.this,CustomerSignUpActivity.this, getParent());
        scheduleRequest.SocialLogin(CustomerSignUpActivity.this,social_id,username,fcm_token,email,first_name,last_name,image,latitude,longitude,"4");

        hideProgress();

    }


    @Override
    public void onFetchProgress(HistoryItem historyItem) {

    }

    @Override
    public void onUpdateStatusProgress(UpdateStatus status) {

    }

    @Override
    public void onFetchComplete() {

    }
}
