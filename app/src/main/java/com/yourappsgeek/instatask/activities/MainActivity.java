package com.yourappsgeek.instatask.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.ExifInterface;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;
import com.yourappsgeek.instatask.CircleImageView;
import com.yourappsgeek.instatask.Connection.networkConnection;
import com.yourappsgeek.instatask.Controller.DialogController;
import com.yourappsgeek.instatask.R;
import com.yourappsgeek.instatask.ApiServices.APIService;
import com.yourappsgeek.instatask.RetrofitClient.ApiClient;
import com.yourappsgeek.instatask.Services.Constants;
import com.yourappsgeek.instatask.SharedPreference.SaveSharedPreference;
import com.yourappsgeek.instatask.Singleton.SocketInstance;
import com.yourappsgeek.instatask.config.ImageUri;
import com.yourappsgeek.instatask.fragments.SelectServiceBSFragment;
import com.yourappsgeek.instatask.utils.UiUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.Channels;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Scanner;

import io.socket.client.Ack;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.yourappsgeek.instatask.Controller.internetCheck.isConnected;
import static com.yourappsgeek.instatask.Services.Constants.Social_images;
import static com.yourappsgeek.instatask.Services.Constants.User_ID;
import static com.yourappsgeek.instatask.Services.Constants.dialogFlag;
import static com.yourappsgeek.instatask.Services.Constants.realPath;
import static com.yourappsgeek.instatask.config.ImageUri.getImageUri;
import static com.yourappsgeek.instatask.config.ImageUri.getRealPathFromURI;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback {
    SelectServiceBSFragment bottomSheetFragment = new SelectServiceBSFragment();
    Switch statusSwitch;
    int num = 0;
    float rate;
    long timmerValue, finishValue = 0;
    //Image request code
    private int PICK_IMAGE_REQUEST = 1;
    boolean rating = false;
    boolean offline = false;
    boolean switch_status = false;
    private FirebaseAnalytics mFirebaseAnalytics;

    private int GALLERY = 1, CAMERA = 2;
    //storage permission code
    CircleImageView header_image, user_image, rating_imag;
    private static final int STORAGE_PERMISSION_CODE = 123;
    TextView header_name, rate_value;
    String userID;
    MaterialRatingBar materialRatingBar, header_rating;
    //Bitmap to get image from gallery
    private Bitmap bitmap;
    CountDownTimer yourCountDownTimer;
    //Uri to store the image uri
    private Uri filePath, imgPath;
    private final String[] permissions = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION};
    private static final int REQUEST_CODE = 12345;
    private static final int PERMISSION_REQUEST_CODE = 1;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    TextView tvStatusTitle, tvStatus;
    int jobStatus, approvelStatus, userId, profileStatus;
    String statusFlag, fcm_token, logoutFlag, TAG, result;
    FusedLocationProviderClient mFusedLocationProviderClient;
    private static final float DEFAULT_ZOOM = 15.0f;
    List<Marker> markers;
    Marker marker, marker1;
    Double lati, longi;
    Uri path;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String user_imge = "null";

    private GoogleMap mMap;
    final static int REQUEST_LOCATION = 199;
    IntentFilter mIntent;
    private BroadcastReceiver statusReceiver;

    /*home_bottom_fragment*/
    LinearLayout home_bottom_fragment;

    /*confirm_job_bottom_fragment*/
    ConstraintLayout confirm_job_bottom_fragment;

    //Leave for job
    ConstraintLayout leave_for_job;

    int jobId = 0;
    Button cancelJobbtn, confirmJobComplete;
    TextView customerUserNameTv, customerLocationTv, servicesPriceTv, lanevaleTv;
    TextView customerUserNameTv1, customerLocationTv1, servicesPriceTv1, lanevaleTv1;
    TextView ratingProviderName, ratingServicesCharges;
    TextView completedLocationAddress, completedServiceLane;
    TextView startLocationAddress, startServiceLane;
    TextView arrivaalLocationAddress, arrivalServiceLane;
    TextView service_price, service_name, service_time;
    TextView to_service_price, to_service_name, to_service_time;

    String body, api, title;
    public static boolean check11 = true;
    boolean statusFlag11;
    /*confirm_arrival_bottom_fragment*/
    Button message;
    ConstraintLayout confirm_arrival_bottom_fragment;

    /*job_started_bottom_fragment*/
    ConstraintLayout job_started_bottom_fragment;
    AppCompatImageView imageView;
    public ArrayList historyItems;

    /*job_completed_bottom_fragment*/
    ConstraintLayout job_completed_bottom_fragment;
    TextView request_approvel;
    AppCompatImageView imageView41;
    View headerview;

    /*request_rating_bottom_fragment*/
    Button confirmBtn;
    ConstraintLayout request_rating_bottom_fragment;
    networkConnection connection;
    private Bundle mExtras;
    Socket mSocket;
    SocketInstance instance;

    //    _________________________________________/**/
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


//        if(!isConnected(MainActivity.this)) internetCheck.buildDialog(MainActivity.this).show();
//        else {
        setContentView(R.layout.activity_main);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        connection = new networkConnection(getApplicationContext());
        connection.registerNetworkBroadcastForNougat(this);
        instance = (SocketInstance) getApplication();
        mSocket = instance.getSocketInstance().connect();
        if (ActivityCompat.checkSelfPermission(MainActivity.this, permissions[0]) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(MainActivity.this, permissions[1]) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(MainActivity.this, permissions[2]) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(MainActivity.this, permissions[3]) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, permissions, REQUEST_CODE);
        }

        initView();


        try {
            body = Constants.body;
            api = Constants.api;
            title = Constants.title;


            if (body != null && api != null) {
                notificationData(api, body, title);
                Log.e("notificarionData", api);
                if (api.contains("editRequestStatus")) {

                    DialogController.showDialog(this);


                }
                Constants.title = null;
                Constants.api = null;
                Constants.body = null;
            } else if (Constants.mesg != null && Constants.mesg.equals("You have new messages")) {
                startActivity(new Intent(getApplicationContext(), ChatActivity.class));
                finish();
            } else {
                if (!isConnected(MainActivity.this)) {
                    dialogFlag = false;
                    // internetCheck.ShowSnakeBar(findViewById(R.id.snakebar));
                } else {
                    checkCurrentJob(userID, "1");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        materialRatingBar = findViewById(R.id.ratingBar5);
        //   homeBottomFragmentView();

        confirmJobBottomFragment();

        confirmArrivalBottomFragment();

        jobStartedBottomFragment();

        jobCompletedBottomFragment();

        requestRatingBottomFragment();

        SwitchStatus();


        leave_for_job();


        rate_value = headerview.findViewById(R.id.rate_value);
        findViewById(R.id.btnSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                showBottomSheetDialogFragment();
                if (!isConnected(MainActivity.this)) {
                } else {
                    if (jobId != 0) {
                        float getrating = materialRatingBar.getRating();
                        Rating(jobId, getrating);

                    } else {
                        Toast.makeText(MainActivity.this, "Job Finish", Toast.LENGTH_SHORT).show();
                    }
                }


            }
        });
//////////////////////////////////////////////////*End*/////////////////////////////////////////////
//        showBottomSheetDialogFragment();


        statusSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {

                    if (!isConnected(MainActivity.this)) {
                    } else {

                        workStatusUpdate(0, userId, fcm_token);

                        statusFlag11 = true;
                    }

                } else {
                    if (!isConnected(MainActivity.this)) {
                        dialogFlag = false;
                    } else {

                        workStatusUpdate(3, userId, fcm_token);

                        statusFlag11 = false;
                    }

                }
                if (statusFlag11) {
                    statusSwitch.setOnCheckedChangeListener(null);
                    statusSwitch.setChecked(true);
                    statusSwitch.setOnCheckedChangeListener(this);
                } else {
                    statusSwitch.setOnCheckedChangeListener(null);
                    statusSwitch.setChecked(false);
                    statusSwitch.setOnCheckedChangeListener(this);
                }
            }
        });


        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                num = 1;
                getImage();
            }
        });

        imageView41.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                num = 2;
                editor.putBoolean("job_approved", false).commit();
                getImage();
            }
        });
        // }


    }


    private void initView() {
        markers=new ArrayList<>();
        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        checkAndRequestPermissions();
        fcm_token = SaveSharedPreference.getFCM_Token(getApplicationContext());

        Constants.progressDialog = UiUtils.getProgressDialog(MainActivity.this);
        home_bottom_fragment = findViewById(R.id.home_bottom_fragment);

        service_price = findViewById(R.id.textView262);
        service_name = findViewById(R.id.textView252);
        service_time = findViewById(R.id.textView242);

        to_service_price = findViewById(R.id.earning);
        to_service_name = findViewById(R.id.textView25);
        to_service_time = findViewById(R.id.textView24);
        confirm_job_bottom_fragment = findViewById(R.id.confirm_job_bottom_fragment);
        leave_for_job = findViewById(R.id.leave_for_job_bottom_fragment);
        cancelJobbtn = findViewById(R.id.cancel_job_btn);
        customerUserNameTv = findViewById(R.id.customer_name);
        customerUserNameTv1 = findViewById(R.id.customer_name1);
        customerLocationTv = findViewById(R.id.customer_location);
        customerLocationTv1 = findViewById(R.id.customer_location1);
        servicesPriceTv = findViewById(R.id.service_price);
        servicesPriceTv1 = findViewById(R.id.service_price1);
        lanevaleTv = findViewById(R.id.lane_qntity);
        lanevaleTv1 = findViewById(R.id.lane_qntity1);

        ratingProviderName = findViewById(R.id.rating_provider_name);
        ratingServicesCharges = findViewById(R.id.rating_services_charges);
        completedLocationAddress = findViewById(R.id.completed_location_address);
        completedServiceLane = findViewById(R.id.completed_service_lane);
        startLocationAddress = findViewById(R.id.start_location_address);
        startServiceLane = findViewById(R.id.start_services_lane);
        arrivaalLocationAddress = findViewById(R.id.arrival_location_address);
        arrivalServiceLane = findViewById(R.id.arrival_services_lane);
        confirm_arrival_bottom_fragment = findViewById(R.id.confirm_arrival_bottom_fragment);
        job_started_bottom_fragment = findViewById(R.id.job_started_bottom_fragment);
        job_completed_bottom_fragment = findViewById(R.id.job_completed_bottom_fragment);
        request_rating_bottom_fragment = findViewById(R.id.request_rating_bottom_fragment);
        request_approvel = findViewById(R.id.textV);
        imageView = findViewById(R.id.textView41);
        imageView41 = findViewById(R.id.imageView41);
        Toolbar toolbar = findViewById(R.id.toolbar);
        tvStatusTitle = findViewById(R.id.tvStatusTitle);
        statusSwitch = findViewById(R.id.switch_status);
        tvStatus = findViewById(R.id.status_tv);
        confirmJobComplete = findViewById(R.id.confirmJobCompletedBtn);
        user_image = findViewById(R.id.user_image);
        rating_imag = findViewById(R.id.circleImageView5);
        message = findViewById(R.id.button5);


        editor = preferences.edit();
        boolean state = preferences.getBoolean("online", false);
        if (state) {
            statusSwitch.setChecked(state);
            tvStatusTitle.setText("Online");
        } else {
            statusSwitch.setChecked(state);
            tvStatusTitle.setText("Offline");
        }

        jobStatus = preferences.getInt("job_Status", 0);
        approvelStatus = preferences.getInt("license_Status", 0);
        profileStatus = preferences.getInt("profile_Status", 0);
        userId = preferences.getInt("user_id", 0);
        Constants.userId = userId;
        userID = String.valueOf(userId);

        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        String name = preferences.getString("name", "");
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        headerview = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);
        header_name = headerview.findViewById(R.id.header_name);
        header_image = headerview.findViewById(R.id.imageView);
        header_rating = headerview.findViewById(R.id.headerRating);
        Social_images = preferences.getString("Social_images", "");
//        Constants.profile_image = preferences.getString("profile_image", "");
        String pro_img = preferences.getString("profile_image", "");
        if (!pro_img.equals("")) {
            String img = ApiClient.IMAGE_BASE_URL + pro_img;
            Glide.with(getApplicationContext()).load(img).into(header_image);
        } else if (!Social_images.equals("")) {
            Glide.with(getApplicationContext()).load(Social_images).into(header_image);
        } else {
            Glide.with(getApplicationContext()).load(R.drawable.ic_profile_placeholder).into(header_image);
        }
        header_name.setText(name);


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        Message();

    }

    private void Message() {
        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ChatActivity.class));
            }
        });

    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (check11) {
                api = intent.getStringExtra("API");
                body = intent.getStringExtra("BODY");
                title = intent.getStringExtra("TITLE");
                Log.e("notificarionData", body);
                if (api != null && body != null && title != null) {
                    Log.e("notificarionData", api);
                    if (api.contains("editRequestStatus")) {
                        DialogController.showDialog(MainActivity.this);
                    } else {
                        notificationData(api, body, title);
                    }
                } else if (body.contains("approved")) {
                    notificationData(api, body, title);
                } else if (body.contains("jobDismiss")) {
                    confirm_job_bottom_fragment.setVisibility(View.GONE);
                    home_bottom_fragment.setVisibility(View.GONE);
                }

            }
        }
    };

    private void notificationData(String api, String body, String title) {

        try {
            if (api != null) {
                if (api.contains("checkJobById_")) {
                    Scanner in = new Scanner(api).useDelimiter("[^0-9]+");
                    jobId = in.nextInt();
                    rating = false;
                    showNotification(title, String.valueOf(jobId));
                    checkJobById(String.valueOf(jobId));
                    home_bottom_fragment.setVisibility(View.GONE);
                } else if (api.contains("rating")) {
                    rating = true;
                    Scanner in = new Scanner(body).useDelimiter("[^0-9]+");
                    int job_id = in.nextInt();
                    checkJobById(String.valueOf(job_id));
                } else if (api.equals("checkCurrentJob")) {

                    checkCurrentJob(userID, "1");

                }
            }
            if (body.equals("unapproved")) {
                tvStatus.setVisibility(View.VISIBLE);
                editor.putBoolean("switchStatus", false).commit();
                SwitchStatus();
            } else if (body.equals("approved")) {
                tvStatus.setVisibility(View.GONE);
                editor.putBoolean("switchStatus", true).commit();
                SwitchStatus();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SwitchStatus() {
        switch_status = preferences.getBoolean("switchStatus", false);

        if (switch_status) {

            tvStatus.setVisibility(View.GONE);
            statusSwitch.setClickable(true);
            statusSwitch.setEnabled(true);
            //  Toast.makeText(getApplicationContext(), "Profile Approved by Admin!", Toast.LENGTH_SHORT).show();
            check11 = false;
        } else {

            check11 = false;

            tvStatus.setVisibility(View.VISIBLE);
            tvStatus.setText(R.string.approvelNotes);
            statusSwitch.setChecked(false);
            tvStatusTitle.setText(R.string.offline);
            statusSwitch.setEnabled(false);
        }

        hideProgress();
    }

    public void limitation() {

        APIService request = ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call = request.limitations();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject json = new JSONObject(response.body().string());
                    Log.d("Response", json + "");
                    int code = json.getInt("code");
                    if (json.get("data") instanceof JSONObject && code == 200) {

                        JSONObject data = json.getJSONObject("data");
                        int second = data.getInt("max_job_acception_time");

                        timmerValue = second * 1000;

                        yourCountDownTimer = new CountDownTimer(30000, timmerValue) {

                            public void onTick(long millisUntilFinished) {
                                // mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                                long time = timmerValue / 1000;
                                Log.e("timmer", String.valueOf(time));
                                home_bottom_fragment.setVisibility(View.GONE);
                                confirm_job_bottom_fragment.setVisibility(View.VISIBLE);
                                confirm_job_bottom_fragment.bringToFront();
//                                Toast.makeText(MainActivity.this, String.valueOf(time), Toast.LENGTH_SHORT).show();

                            }

                            public void onFinish() {
                                confirm_job_bottom_fragment.setVisibility(View.GONE);
                                home_bottom_fragment.setVisibility(View.VISIBLE);
                                Log.e("timmer", "finish");
                                statusSwitch.setClickable(true);


                                // mTextField.setText("done!");
                            }
                        }.start();

//                        yourCountDownTimer = new CountDownTimer(30000, timmerValue) {
//
//                            public void onTick(long millisUntilFinished) {
//                                finishValue++;
//                                Log.d("timmer", String.valueOf(finishValue));
//                            }
//
//                            public void onFinish() {
//                                if (finishValue == 1) {
//                                    finishValue = 0;
//
//                                }
//                            }
//                        }.start();


                    } else {
                        Toast.makeText(getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();

                    }
                    hideProgress();
                } catch (JSONException e) {

                    e.printStackTrace();
                } catch (IOException e) {

                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }


    private void jobCompletedBottomFragment() {
        findViewById(R.id.confirmJobStartBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                showBottomSheetDialogFragment();
                if (realPath != null) {
                    if (!isConnected(MainActivity.this)) {
                    } else {
                        File file = new File(realPath);
                        // Create a request body with file and image media type
                        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
                        // Create MultipartBody.Part using file request-body,file name and part name
                        MultipartBody.Part part = MultipartBody.Part.createFormData("current_situation_img", file.getName(), fileReqBody);
                        realPath = null;
                        updateStatus(5, jobId, part);
                        Log.e("images", String.valueOf(body));
                    }

                } else {
                    Toast.makeText(MainActivity.this, "Please Add image!", Toast.LENGTH_SHORT).show();
                }


            }
        });
    }

    private void requestRatingBottomFragment() {
        confirmJobComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Create a file object using file path
                if (!isConnected(MainActivity.this)) {

                } else {
                    if (realPath != null) {
                        File file = new File(realPath);
                        // Create a request body with file and image media type
                        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
                        // Create MultipartBody.Part using file request-body,file name and part name
                        MultipartBody.Part part = MultipartBody.Part.createFormData("after_work_img", file.getName(), fileReqBody);
                        realPath = null;
                        job_approvel_request(jobId, userId, part);
                    } else {
                        Toast.makeText(MainActivity.this, "Please add image!", Toast.LENGTH_SHORT).show();
                    }
                }


            }
        });
    }

    private void jobStartedBottomFragment() {
        findViewById(R.id.confirmArrivalBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                showBottomSheetDialogFragment();

                if (!isConnected(MainActivity.this)) {
                } else {
                    updateStatus(7, jobId, null);

                }

            }
        });
    }

    private void confirmArrivalBottomFragment() {
        findViewById(R.id.confirmJobBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!isConnected(MainActivity.this)) {
                } else {
                    AcceptJob(body, userId);
                }

            }
        });
    }

    private void leave_for_job() {
        findViewById(R.id.leaveJobBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isConnected(MainActivity.this)) {
                } else {
                    updateStatus(1, jobId, null);

                }

            }
        });
    }

    private void confirmJobBottomFragment() {

        cancelJobbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isConnected(MainActivity.this)) {
                } else {
                    if (String.valueOf(jobId) != null) {

                        rejectJob(userId, jobId);
//                        cancelJob(String.valueOf(jobId));
                    } else {
                        Toast.makeText(MainActivity.this, "JobId Not Found", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_profile) {
            startActivity(new Intent(MainActivity.this, ProfileActivity.class));
            finish();

        } else if (id == R.id.nav_my_request) {
            startActivity(new Intent(MainActivity.this, RequestActivity.class));

        } else if (id == R.id.nav_payment) {
            startActivity(new Intent(MainActivity.this, PaymentActivity.class));

        } else if (id == R.id.nav_help) {
            startActivity(new Intent(MainActivity.this, HelpActivity.class));
        } else if (id == R.id.nav_logout) {

            logoutFlag = preferences.getString("socialIdentity", "null");
            if (logoutFlag.equals("fb@g.c")) {
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                preferences.edit().clear().apply();
                finish();
            } else if (logoutFlag.equals("twitter@g.c")) {
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                preferences.edit().clear().apply();
                finish();
            } else {
                if (!isConnected(MainActivity.this)) {
                } else {
                    if (jobId == 0) {
                        tvStatusTitle.setText(R.string.offline);
                        editor.putBoolean("online", false).commit();
                        statusFlag11 = false;
                        logout(fcm_token);
                    } else {
                        Toast.makeText(this, "You cannot logout while you are on a job.", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        List<Marker> markers = new ArrayList<>();
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Log.i("Latitude", latLng.latitude + "\nlonitude" + latLng.longitude);
            }
        });
    }

//    public void showBottomSheetDialogFragment() {
//        if (!bottomSheetFragment.isVisible()) {
//            bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
//        }
//
//    }

    public void logout(String fb_token) {

        try {
            Constants.progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

        APIService request = ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call = request.userLogout(fb_token);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {
                            JSONObject data = json.getJSONObject("data");
                            Toast.makeText(MainActivity.this, data.getString("status"), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                            editor.putString("Social_images", "").apply();
                            editor.putString("profile_image", "").apply();
                            SaveSharedPreference.setLoggedIn(getApplicationContext(), false);
                            finish();
                        } else if (code == 405) {
                            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                            finish();
                            SaveSharedPreference.setLoggedIn(getApplicationContext(), false);
                            editor.putString("Social_images", "").apply();
                            editor.putString("profile_image", "").apply();
                            Toast.makeText(getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();

                        }
                    } else {
                        Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();
                } catch (JSONException e) {

                    e.printStackTrace();
                } catch (IOException e) {

                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }


    public void workStatusUpdate(final int statusValue, int use_d, String fb_token) {

        try {
            Constants.progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        APIService request = ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call = request.workStatusUpdate(statusValue, use_d, fb_token);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {
                            if (statusValue == 0) {
                                tvStatusTitle.setText(R.string.online);

                                editor.putBoolean("online", true).commit();
                            } else if (statusValue == 3) {
                                tvStatusTitle.setText(R.string.offline);
                                editor.putBoolean("online", false).commit();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();

                        }
                    } else {
                        Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }

                    hideProgress();
                } catch (JSONException e) {

                    e.printStackTrace();
                } catch (IOException e) {

                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }


    private void getDeviceLocation() {

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        try {
//            if (mLocationPermissionsGranted) {

            final Task location = mFusedLocationProviderClient.getLastLocation();
            location.addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    try {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "onComplete: found location!");
                            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                            Location currentLocation = (Location) task.getResult();

                            try {
                                lati = currentLocation.getLatitude();
                                longi = currentLocation.getLongitude();
                                moveCamera(new LatLng(lati, longi), DEFAULT_ZOOM);

                                List<Address> addressList = geocoder.getFromLocation(lati, longi, 1);
                                if (addressList != null && addressList.size() > 0) {
                                    Address address = addressList.get(0);
                                    StringBuilder sb = new StringBuilder();
                                    for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                                        sb.append(address.getAddressLine(i)).append("\n");
                                    }
                                    sb.append(address.getAddressLine(0));
                                    result = sb.toString();
                                    Log.e("Unable", result);
                                    // Toast.makeText(MainActivity.this, result, Toast.LENGTH_SHORT).show();
                                    if (lati != null && longi != null) {
                                        if (!isConnected(MainActivity.this)) {
                                        } else {
                                            latLogUpdate(lati, longi, userId, fcm_token);
                                        }
                                    }
                                }
                            } catch (IOException e) {
                                Log.e(TAG, "Unable connect to Geocoder", e);
                            }

                        } else {
                            Log.d(TAG, "onComplete: current location is null");
                            Toast.makeText(MainActivity.this, "unable to get current location", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception ex) {
                        Toast.makeText(MainActivity.this, "getLocation on nul reference", Toast.LENGTH_SHORT).show();
                    }
                }
            });
//            }
        } catch (SecurityException e) {
            Log.e(TAG, "getDeviceLocation: SecurityException: " + e.getMessage());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: called.");
//        mLocationPermissionsGranted = false;

        if (requestCode == REQUEST_CODE) {
            if (grantResults.length == 4 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED && grantResults[3] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(MainActivity.this, "Permission granted!!!", Toast.LENGTH_LONG).show();
                initMap();
            } else {
                Toast.makeText(MainActivity.this, "Necessary permissions not granted...", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }


    private void moveCamera(LatLng latLng, float zoom) {
        Log.d(TAG, "moveCamera: moving the camera to: lat: " + latLng.latitude + ", lng: " + latLng.longitude);
        // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
//        if (marker != null) {
//            marker.remove();
//        }
//        marker = mMap.addMarker(new MarkerOptions().position(latLng).title("My Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_marker)));
//        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
        JSONObject object = new JSONObject();
        try {
            object.put("lat", latLng.latitude);
            object.put("lng", latLng.longitude);
            mSocket.emit("getDriversData", object, new Ack() {
                @Override
                public void call(Object... args) {
                    JSONObject position = (JSONObject) args[0];
                    Log.i("callback", String.valueOf(position));
                }
            });
            //setListening();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setListening();

    }


//    private void moveCamera(LatLng latLng, float zoom) {
//        if (markers != null) {
//            mMap.clear();
//            markers.clear();
//        }
//        Log.d(TAG, "moveCamera: moving the camera to: lat: " + latLng.latitude + ", lng: " + latLng.longitude);
//        // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
//        markers = new ArrayList<>();
////
////        // Add a marker in Sydney and move the camera
//        marker = mMap.addMarker(new MarkerOptions().position(latLng).title("My Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_marker)));
//
//        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 20));
//
//        markers.add(marker);
//    }


    private void initMap() {
        Log.d(TAG, "initMap: initializing map");
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        mapFragment.getMapAsync(MainActivity.this);
        getDeviceLocation();

    }

    public void latLogUpdate(Double lat, Double log, int id, String fb_token) {

        APIService request = ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call = request.userProfileUpdateLatiLogi(id, fb_token, lat, log);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response4", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {

                            JSONObject data = json.getJSONObject("data");
                            // Toast.makeText(MainActivity.this, data.getString("status"), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();

                        }
                    } else {
                        Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();
                } catch (JSONException e) {
                    e.printStackTrace();


                } catch (IOException e) {
                    e.printStackTrace();


                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
                hideProgress();
            }
        });
    }

    void hideProgress() {
        if (Constants.progressDialog != null && Constants.progressDialog.isShowing())
            Constants.progressDialog.dismiss();
    }


    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

    public void checkJobById(String JOB_ID) {
        try {
            Constants.progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        check11 = false;
        APIService request = ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call = request.checkJobById(JOB_ID);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {
                            //    startActivity(new Intent(MainActivity.this,MainActivity.class));

                            statusSwitch.setClickable(false);
                            JSONObject data = json.getJSONObject("data");
                            JSONObject user = data.getJSONObject("user");

                            Constants.userAddress = data.getString("location_address");
                            Constants.service_type = data.getString("service_name");
                            Constants.service_price = data.getString("service_price");
                            Constants.userName = user.getString("first_name") + " " + user.getString("last_name");


                            if (rating) {
                                job_completed_bottom_fragment.setVisibility(View.GONE);
                                confirm_job_bottom_fragment.setVisibility(View.GONE);
                                request_rating_bottom_fragment.setVisibility(View.VISIBLE);
                                home_bottom_fragment.setVisibility(View.GONE);
                                if (user_imge != null) {
                                    Glide.with(getApplicationContext()).load(ApiClient.IMAGE_BASE_URL + user_imge).into(rating_imag);
                                } else {
                                    Glide.with(getApplicationContext()).load(R.drawable.ic_profile_placeholder).into(rating_imag);
                                }
                                String rate_val = preferences.getString("rate_value", "");
                                if (!rate_val.equals("")) {
                                    materialRatingBar.setRating(Float.parseFloat(rate_val));
                                }
                                setFragmentData();
                            } else {
                                editor.putBoolean("job_approved", false).commit();
                                home_bottom_fragment.setVisibility(View.GONE);
                                confirm_job_bottom_fragment.setVisibility(View.VISIBLE);
                                confirm_job_bottom_fragment.bringToFront();
                                limitation();
                                setFragmentData();
                            }

                            // Toast.makeText(MainActivity.this, json.getString("msg"), Toast.LENGTH_SHORT).show();


                        } else {
                            Toast.makeText(getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();

                        }
                    } else {
                        Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();
                } catch (JSONException e) {
                    e.printStackTrace();


                } catch (IOException e) {
                    e.printStackTrace();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
                hideProgress();
            }
        });
    }


    public void checkCurrentJob(String user_id, String type) {
        try {
            Constants.progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        check11 = false;
        APIService request = ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call = request.checkCurrentJob(user_id, type);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        JSONObject job = null;
                        double provider_rating = 0;
                        JSONObject customer = null;
                        int jobStatus = -1;
                        if (json.get("data") instanceof JSONObject && code == 200) {

                            JSONObject data = json.getJSONObject("data");
                            String today_earning = data.getString("todayJobEarnings");
                            String today_trips = data.getString("todayJobCount");
                            JSONObject providerTimeLogs = data.getJSONObject("providerTimeLogs");
                            long todayWorkingHours = providerTimeLogs.getLong("todayWorkingHours");
                            long lastLoginTime = providerTimeLogs.getLong("lastLoginTime");

                            TodayTotal(today_earning, today_trips, todayWorkingHours, lastLoginTime);

                            JSONObject providerDetails = data.getJSONObject("providerDetails");
                            int approval_status = providerDetails.getInt("approval_status");

                            if (approval_status == 1) {
                                editor.putBoolean("switchStatus", false).commit();
                                SwitchStatus();
                            } else if (approval_status == 0) {

                                editor.putBoolean("switchStatus", true).commit();
                                SwitchStatus();
                            }


                            if (data.has("job")) {
                                job = data.getJSONObject("job");
                                jobStatus = job.getInt("job_status");
                                customer = job.getJSONObject("user");
                                providerDetails = job.getJSONObject("provider");
                                Constants.User_ID = job.getString("customer_id");
                                Constants.Provider_ID = job.getString("provider_id");
                                provider_rating = providerDetails.getDouble("rating");
                                Constants.userName = customer.getString("first_name") + " " + customer.getString("last_name");
                                Constants.userAddress = job.getString("location_address");
                                Constants.service_type = job.getString("service_name");
                                Constants.service_price = job.getString("service_price");
                                if (customer.has("avatar") && !customer.isNull("avatar")) {
                                    user_imge = customer.getString("avatar");
                                    String img = ApiClient.IMAGE_BASE_URL + user_imge;
                                    Glide.with(getApplicationContext()).load(img).into(user_image);
                                } else {
                                    user_imge = "null";
                                }
                                jobId = job.getInt("id");
                                Constants.Job_Id = jobId;
                            } else if (data.has("currentDispatchedJob")) {
                                JSONObject currentDispatchedJob = data.getJSONObject("currentDispatchedJob");
                                if (currentDispatchedJob != null && currentDispatchedJob.length() > 0) {
                                    statusSwitch.setClickable(false);
                                    JSONObject user = currentDispatchedJob.getJSONObject("user");
                                    body = currentDispatchedJob.getString("id");
                                    Constants.userAddress = currentDispatchedJob.getString("location_address");
                                    Constants.service_type = currentDispatchedJob.getString("service_name");
                                    Constants.service_price = currentDispatchedJob.getString("service_price");
                                    Constants.userName = user.getString("first_name") + " " + user.getString("last_name");
                                    Log.e("despatch", "job");
                                    editor.putBoolean("job_approved", false).commit();
                                    home_bottom_fragment.setVisibility(View.GONE);
                                    confirm_job_bottom_fragment.setVisibility(View.VISIBLE);
                                    confirm_job_bottom_fragment.bringToFront();
                                    limitation();
                                } else {
                                    jobStatus = data.getInt("job_status");
                                    providerDetails = data.getJSONObject("user");
                                    Constants.userName = providerDetails.getString("first_name") + " " + providerDetails.getString("last_name");
                                    provider_rating = providerDetails.getDouble("rating");

                                }
                            }

                            if (data.has("user") && !data.isNull("user")) {

                                customer = data.getJSONObject("user");

                                if (customer.has("fcm_token") && customer.isNull("fcm_token")) {
                                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                                    SaveSharedPreference.setLoggedIn(getApplicationContext(), false);
                                    finish();
                                } else if (customer.has("fcm_token") && !customer.isNull("fcm_token")) {
                                    String token = customer.getString("fcm_token");
                                    if (!fcm_token.equals(token)) {
                                        startActivity(new Intent(MainActivity.this, LoginActivity.class));
                                        SaveSharedPreference.setLoggedIn(getApplicationContext(), false);
                                        finish();
                                    }
                                }

                            }

                            if (data.has("lastJob")) {
                                JSONObject lastjob = data.getJSONObject("lastJob");
                                if (lastjob != null && lastjob.length() > 0) {
                                    String price = lastjob.getString("service_price");
                                    String lane = lastjob.getString("service_name");
                                    long time = lastjob.getLong("job_schedual_time");

                                    lastJob(price, lane, time);
                                }

                            }

                            if (jobStatus != -1) {
                                if (jobStatus != 3 && jobStatus != 6) {
                                    if (!mSocket.connected()) {
                                        instance.getSocketInstance().connect();
                                    }

                                    double lati = customer.getDouble("lat");
                                    double longi = customer.getDouble("lng");
                                    JSONObject provider = job.getJSONObject("provider");
                                    double latitude = provider.getDouble("lat");
                                    double longitude = provider.getDouble("lng");
                                    LatLng Provoder = new LatLng(latitude, longitude);
                                    LatLng Customer = new LatLng(lati, longi);
                                    googleMapBounds(Provoder,Customer);
                                }
                            }


                            if (provider_rating != 0.0 || provider_rating != 0) {
                                DecimalFormat df = new DecimalFormat("0.00");
                                float rate = Float.parseFloat(String.format(df.format(provider_rating)));
                                header_rating.setRating(rate);
                                rate_value.setText(rate + "/5.0");
                                editor.putString("rate_value", rate + "").commit();
                                Log.e("rate", rate + "");
                            } else {
                                header_rating.setRating(5.0f);
                                rate_value.setText(5.0 + "/5.0");
                            }


                            switch (jobStatus) {
                                case 0:/*complete */
                                    request_rating_bottom_fragment.setVisibility(View.VISIBLE);
                                    home_bottom_fragment.setVisibility(View.GONE);
                                    confirm_job_bottom_fragment.setVisibility(View.GONE);
                                    confirm_arrival_bottom_fragment.setVisibility(View.GONE);
                                    job_completed_bottom_fragment.setVisibility(View.GONE);
                                    job_started_bottom_fragment.setVisibility(View.GONE);
                                    editor.putBoolean("switchStatus", true).commit();

                                    statusSwitch.setClickable(false);
                                    break;
                                case 1:/*leave_for_job  */
                                    confirm_arrival_bottom_fragment.setVisibility(View.VISIBLE);
                                    home_bottom_fragment.setVisibility(View.GONE);
                                    confirm_job_bottom_fragment.setVisibility(View.GONE);
                                    job_completed_bottom_fragment.setVisibility(View.GONE);
                                    job_started_bottom_fragment.setVisibility(View.GONE);
                                    request_rating_bottom_fragment.setVisibility(View.GONE);
                                    statusSwitch.setClickable(false);
                                    break;
                                case 2:/*pending  */
                                    confirm_job_bottom_fragment.setVisibility(View.VISIBLE);
                                    leave_for_job.setVisibility(View.GONE);
                                    home_bottom_fragment.setVisibility(View.GONE);
                                    confirm_arrival_bottom_fragment.setVisibility(View.GONE);
                                    job_completed_bottom_fragment.setVisibility(View.GONE);
                                    job_started_bottom_fragment.setVisibility(View.GONE);
                                    statusSwitch.setClickable(false);
                                    limitation();
                                    break;
                                case 3:/*cancel  */
                                    Log.e("finish", "cancel");
                                    editor.putBoolean("switchStatus", true).commit();
                                    home_bottom_fragment.setVisibility(View.VISIBLE);
                                    confirm_job_bottom_fragment.setVisibility(View.GONE);
                                    confirm_arrival_bottom_fragment.setVisibility(View.GONE);
                                    job_completed_bottom_fragment.setVisibility(View.GONE);
                                    job_started_bottom_fragment.setVisibility(View.GONE);
                                    request_rating_bottom_fragment.setVisibility(View.GONE);
                                    leave_for_job.setVisibility(View.GONE);
                                    statusSwitch.setClickable(true);
                                    break;
                                case 4:/*accept  */

                                    leave_for_job.setVisibility(View.VISIBLE);
                                    home_bottom_fragment.setVisibility(View.GONE);
                                    confirm_job_bottom_fragment.setVisibility(View.GONE);
                                    confirm_arrival_bottom_fragment.setVisibility(View.GONE);
                                    job_completed_bottom_fragment.setVisibility(View.GONE);
                                    job_started_bottom_fragment.setVisibility(View.GONE);
                                    statusSwitch.setClickable(false);
                                    if (!user_imge.equals("null")) {
                                        String img = ApiClient.IMAGE_BASE_URL + user_imge;
                                        Glide.with(getApplicationContext()).load(img).into(user_image);
                                    } else {
                                        Glide.with(getApplicationContext()).load(R.drawable.ic_profile_placeholder).into(user_image);
                                    }
                                    break;
                                case 5:/*working  */
                                    boolean approve = preferences.getBoolean("job_approved", false);
                                    if (approve) {
                                        imageView41.setImageBitmap(null);
                                        imageView41.setBackgroundResource(R.drawable.no_uploaded);
                                        request_approvel.setText("Customer disapproved work!");
                                        confirmJobComplete.setEnabled(true);
                                        home_bottom_fragment.setVisibility(View.GONE);
                                        job_completed_bottom_fragment.setVisibility(View.VISIBLE);
                                        request_approvel.setBackgroundColor(Color.parseColor("#CDEB291B"));
                                    } else {
                                        job_completed_bottom_fragment.setVisibility(View.VISIBLE);
                                        request_approvel.setText("Job Start!");
                                        request_approvel.setBackgroundColor(Color.parseColor("#77B03B"));
                                        home_bottom_fragment.setVisibility(View.GONE);
                                        confirm_job_bottom_fragment.setVisibility(View.GONE);
                                        confirm_arrival_bottom_fragment.setVisibility(View.GONE);
                                        job_started_bottom_fragment.setVisibility(View.GONE);
                                        statusSwitch.setClickable(false);
                                    }
                                    break;
                                case 6:/*timeout  */
                                    home_bottom_fragment.setVisibility(View.GONE);
                                    confirm_job_bottom_fragment.setVisibility(View.GONE);
                                    confirm_arrival_bottom_fragment.setVisibility(View.GONE);
                                    job_completed_bottom_fragment.setVisibility(View.GONE);
                                    job_started_bottom_fragment.setVisibility(View.GONE);
                                    statusSwitch.setClickable(true);
                                    editor.putBoolean("switchStatus", true).commit();
                                    Toast.makeText(MainActivity.this, "Last job Time out ", Toast.LENGTH_SHORT).show();
                                    break;

                                case 7:/*confirm arrival  */
                                    job_started_bottom_fragment.setVisibility(View.VISIBLE);
                                    home_bottom_fragment.setVisibility(View.GONE);
                                    confirm_job_bottom_fragment.setVisibility(View.GONE);
                                    confirm_arrival_bottom_fragment.setVisibility(View.GONE);
                                    job_completed_bottom_fragment.setVisibility(View.GONE);
                                    statusSwitch.setClickable(false);
                                    break;
                                case 8:/* requist approvel */
                                    request_approvel.setText("Send Request for approvel!");
                                    confirmJobComplete.setEnabled(false);
                                    job_completed_bottom_fragment.setVisibility(View.VISIBLE);
                                    home_bottom_fragment.setVisibility(View.GONE);
                                    confirm_job_bottom_fragment.setVisibility(View.GONE);
                                    confirm_arrival_bottom_fragment.setVisibility(View.GONE);
                                    job_started_bottom_fragment.setVisibility(View.GONE);
                                    statusSwitch.setClickable(false);
                                    break;
                            }
//                        showNotification(title, body);
                            Toast.makeText(MainActivity.this, json.getString("msg"), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();

                        }
                    } else {
                        Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                    setFragmentData();
                    hideProgress();
                } catch (JSONException e) {
                    e.printStackTrace();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
                hideProgress();
            }
        });
    }

    public void cancelJob(String jobId) {
        APIService request = ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call = request.cancelJob(jobId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject json = new JSONObject(response.body().string());
                    Log.d("Response", json + "");
                    int code = json.getInt("code");
                    if (json.get("data") instanceof JSONObject && code == 200) {
                        editor.putString("jobid", "").commit();
                        home_bottom_fragment.setVisibility(View.VISIBLE);
                        confirm_job_bottom_fragment.setVisibility(View.GONE);
                        editor.putBoolean("switchStatus", true).commit();
                        JSONObject data = json.getJSONObject("data");
                        Toast.makeText(MainActivity.this, json.getString("success_msg"), Toast.LENGTH_SHORT).show();
                        hideProgress();
                    } else {
                        Toast.makeText(MainActivity.this, json.getString("msg"), Toast.LENGTH_LONG).show();
                        hideProgress();
                    }
                } catch (JSONException e) {
                    hideProgress();
                    e.printStackTrace();
                } catch (IOException e) {
                    hideProgress();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }

    private void AcceptJob(String jobID, int driverID) {

        APIService request = ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call = request.acceptjob(jobID, driverID);
        try {
            Constants.progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {
                            JSONObject data = json.getJSONObject("data");
                            String id = data.getString("id");
                            if (!preferences.getString("jobid", "").equals("")) {
                                if (!id.equals(preferences.getString("jobid", ""))) {
                                    confirm_job_bottom_fragment.setVisibility(View.GONE);
                                    leave_for_job.setVisibility(View.VISIBLE);
                                    yourCountDownTimer.cancel();
                                }
                            } else {
                                editor.putString("jobid", id).commit();
                                confirm_job_bottom_fragment.setVisibility(View.GONE);
                                leave_for_job.setVisibility(View.VISIBLE);
                                yourCountDownTimer.cancel();

                            }

                        } else {
                            Toast.makeText(MainActivity.this, json.getString("msg"), Toast.LENGTH_LONG).show();

                        }
                    } else {

                        Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }

    private void Rating(int jobID, float rating) {


        try {
            Constants.progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

        APIService request = ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call = request.rating(jobID, rating);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {


                            editor.putBoolean("job_approved", false).commit();
                            request_rating_bottom_fragment.setVisibility(View.GONE);
                            Log.e("finish", "ratingbar");
                            home_bottom_fragment.setVisibility(View.VISIBLE);
                        } else {
                            Toast.makeText(MainActivity.this, json.getString("msg"), Toast.LENGTH_LONG).show();

                        }
                    } else {

                        Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();
                } catch (JSONException e) {

                    e.printStackTrace();
                } catch (IOException e) {

                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void updateStatus(final int jobStatus, final int job_ID, MultipartBody.Part image) {


        APIService request = ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call = request.updateStatus(jobStatus, job_ID, image);
        Log.e("calldata", String.valueOf(image));
        try {
            Constants.progressDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {
                            if (jobStatus == 1) {
                                leave_for_job.setVisibility(View.GONE);
                                confirm_arrival_bottom_fragment.setVisibility(View.VISIBLE);
                            } else if (jobStatus == 7) {

                                confirm_arrival_bottom_fragment.setVisibility(View.GONE);
                                job_started_bottom_fragment.setVisibility(View.VISIBLE);

                            } else if (jobStatus == 5) {
                                path = null;
                                job_started_bottom_fragment.setVisibility(View.GONE);
                                job_completed_bottom_fragment.setVisibility(View.VISIBLE);
                                imageView.setImageBitmap(null);
                                imageView.setBackgroundResource(R.drawable.no_uploaded);
                            } else if (jobStatus == 0) {
                                job_completed_bottom_fragment.setVisibility(View.GONE);
                                request_rating_bottom_fragment.setVisibility(View.GONE);
                                statusSwitch.setClickable(true);
                            }

                        } else {
                            Toast.makeText(MainActivity.this, json.getString("msg"), Toast.LENGTH_LONG).show();

                        }
                    } else {

                        Toast.makeText(MainActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();
                } catch (JSONException e) {

                    e.printStackTrace();
                } catch (IOException e) {

                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void job_approvel_request(final int job_ID, int driver_id, MultipartBody.Part image) {

        APIService request = ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call = request.requestJobApproval(job_ID, driver_id, image);
        try {
            Constants.progressDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {
                            confirmJobComplete.setEnabled(false);
                            request_approvel.setText("Send Request for approvel!");
                            request_approvel.setBackgroundColor(Color.parseColor("#77B03B"));

                            editor.putBoolean("job_approved", true).commit();

                        } else {
                            Toast.makeText(MainActivity.this, json.getString("msg"), Toast.LENGTH_LONG).show();
                            confirmJobComplete.setEnabled(true);

                        }
                    } else {
                        Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                confirmJobComplete.setEnabled(true);
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (isLocationEnabled(getApplicationContext())) {
            getDeviceLocation();
        } else {
            displayLocationSettingsRequest(getApplicationContext());
        }
        LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(broadcastReceiver, new IntentFilter("NOW"));
        checkCurrentJob(userID, "1");

    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }


    }


    private boolean checkAndRequestPermissions() {
        int camera = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
        int storage = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int loc = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        int loc2 = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.CAMERA);
        }
        if (storage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (loc2 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (loc != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mIntent != null) {
            unregisterReceiver(statusReceiver);
            mIntent = null;
        }
    }

    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i(TAG, "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(MainActivity.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

    public void getImage() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {"Select photo from gallery", "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (num == 1) {
            if (requestCode == GALLERY) {
                if (data != null) {
                    Uri contentURI = data.getData();
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                        imageView.setImageBitmap(bitmap);
                        Constants.imagePath = getImageUri(getApplicationContext(), bitmap);
                        realPath = ImageUri.getRealPathFromURI(getApplicationContext(), Constants.imagePath);

                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Failed!", Toast.LENGTH_SHORT).show();
                    }
                }

            } else if (requestCode == CAMERA) {
                Bitmap thumbnail = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");

//
                imageView.setImageBitmap(thumbnail);
                Constants.imagePath = getImageUri(getApplicationContext(), thumbnail);
                realPath = ImageUri.getRealPathFromURI(getApplicationContext(), Constants.imagePath);
            }
        } else if (num == 2) {

            if (requestCode == GALLERY) {
                if (data != null) {
                    Uri contentURI = data.getData();
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                        imageView41.setImageBitmap(bitmap);
                        Constants.imagePath = getImageUri(getApplicationContext(), bitmap);
                        realPath = ImageUri.getRealPathFromURI(getApplicationContext(), Constants.imagePath);

                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Failed!", Toast.LENGTH_SHORT).show();
                    }
                }

            } else if (requestCode == CAMERA) {
                Bitmap thumbnail = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");

//
                imageView41.setImageBitmap(thumbnail);
                Constants.imagePath = getImageUri(getApplicationContext(), thumbnail);
                realPath = ImageUri.getRealPathFromURI(getApplicationContext(), Constants.imagePath);
            }

        }


    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);

        int column_index = 0;
        if (cursor != null) {
            column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        }
        cursor.moveToFirst();

        return cursor.getString(column_index);
    }


    private String getRightAngleImage(String photoPath) {

        try {
            ExifInterface ei = new ExifInterface(photoPath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            int degree = 0;

            switch (orientation) {
                case ExifInterface.ORIENTATION_NORMAL:
                    degree = 0;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degree = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    degree = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    degree = 270;
                    break;
                case ExifInterface.ORIENTATION_UNDEFINED:
                    degree = 0;
                    break;
                default:
                    degree = 90;
            }

            return rotateImage(degree, photoPath);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return photoPath;
    }

    private String rotateImage(int degree, String imagePath) {

        if (degree <= 0) {
            return imagePath;
        }
        try {
            Bitmap b = BitmapFactory.decodeFile(imagePath);

            Matrix matrix = new Matrix();
            if (b.getWidth() > b.getHeight()) {
                matrix.setRotate(degree);
                b = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(),
                        matrix, true);
            }

            FileOutputStream fOut = new FileOutputStream(imagePath);
            String imageName = imagePath.substring(imagePath.lastIndexOf("/") + 1);
            String imageType = imageName.substring(imageName.lastIndexOf(".") + 1);

            FileOutputStream out = new FileOutputStream(imagePath);
            if (imageType.equalsIgnoreCase("png")) {
                b.compress(Bitmap.CompressFormat.PNG, 100, out);
            } else if (imageType.equalsIgnoreCase("jpeg") || imageType.equalsIgnoreCase("jpg")) {
                b.compress(Bitmap.CompressFormat.JPEG, 100, out);
            }
            fOut.flush();
            fOut.close();

            b.recycle();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imagePath;
    }


    void showNotification(String title, String body) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        int notificationId = 1;
        String channelId = "channel-01";
        String channelName = "Channel Name";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext(), channelId)
                .setSmallIcon(R.drawable.app_icon)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(""))
                .setContentText(body)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        Intent intent1 = new Intent(getApplicationContext(), MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
        stackBuilder.addNextIntent(intent1);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        // PendingIntent pIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent1, 0);
        notificationManager.notify(notificationId, mBuilder.build());

    }

    private void setFragmentData() {
        String location, service, price, name;
        location = Constants.userAddress;
        service = Constants.service_type;
        price = Constants.service_price;
        name = Constants.userName;
        ratingProviderName.setText(name);
        ratingServicesCharges.setText("$" + price);
        completedLocationAddress.setText(location);
        completedServiceLane.setText(service);
        startServiceLane.setText(service);
        startLocationAddress.setText(location);
        arrivaalLocationAddress.setText(location);
        arrivalServiceLane.setText(service);
        customerLocationTv.setText(location);
        lanevaleTv.setText(service);
        servicesPriceTv.setText("$" + price);
        customerUserNameTv.setText(name);

        customerLocationTv1.setText(location);
        lanevaleTv1.setText(service);
        servicesPriceTv1.setText("$" + price);
        customerUserNameTv1.setText(name);

    }

    private void lastJob(String price, String lane, long time) {


        service_price.setText("$" + price);
        service_name.setText(lane);

        java.util.Date d = new java.util.Date(time * 1000L);
        String itemDateStr = new SimpleDateFormat("HH:mm").format(d);

        service_time.setText(itemDateStr);


    }


    public void rejectJob(int provider_id, int job_id) {

        try {
            Constants.progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        APIService request = ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call = request.rejectJob(provider_id, job_id);


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {
                    try {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (code == 200) {
//                            JSONArray data=json.getJSONArray("data");
//                            JSONArray job=data.getJSONArray("job");

//                            confirm_job_bottom_fragment.setVisibility(View.GONE);
//                            home_bottom_fragment.setVisibility(View.VISIBLE);
                            checkCurrentJob(userID, "1");
                            hideProgress();
                            Toast.makeText(getApplicationContext(), json.getString("msg"), Toast.LENGTH_SHORT).show();


                        } else {
                            Toast.makeText(getApplicationContext(), json.getString("msg"), Toast.LENGTH_SHORT).show();

                        }
                        hideProgress();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), response.message(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

    }

    @SuppressLint("SetTextI18n")
    private void TodayTotal(String price, String trips, long todayWorkingHours, long lastLoginTime) {

        try {
            long unixTime = System.currentTimeMillis() / 1000L;

            long second = unixTime - lastLoginTime;
            long current_second = second + todayWorkingHours;

            String time = String.valueOf(current_second / 3600);

            to_service_price.setText("$" + price);
            to_service_name.setText(trips + " Trips");
            to_service_time.setText(time + " Hours online");
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void setListening() {
        mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                //  JSONObject position = (JSONObject) args[0];
                Log.e("Response", "connected");

//                mSocket.emit("getDriversData", new LatLng(lat, lng));
//                mSocket.disconnect();
            }

        }).on("getDriversData", new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                Log.e("Response", "getDriversData");
                LatLng value;
                JSONArray array = (JSONArray) args[0];

                try {
                    JSONObject object = array.getJSONObject(0);
                    if (object.has("resolve") && !object.isNull("resolve")) {
                        JSONArray resolve = object.getJSONArray("resolve");
                        if (resolve.length() != 0) {

                            for (int i = 0; i < resolve.length(); i++) {

                                JSONObject provider = resolve.getJSONObject(i);
                                int id = provider.getInt("usr_id");
                                if (id == Integer.valueOf(userID)) {
                                    double provider_lati = provider.getDouble("lat");
                                    double provoder_longi = provider.getDouble("lng");
                                    if (marker != null) {
                                        marker.remove();
                                    }
                                    LatLng latLng = new LatLng(provider_lati, provoder_longi);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            marker = mMap.addMarker(new MarkerOptions().position(latLng).title("My Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_marker)));
                                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM));
                                        }
                                    });

                                }
                            }

//
//                            JSONObject customer = resolve.getJSONObject(1);
//                            double customer_lati = customer.getDouble("lat");
//                            double customer_longi = customer.getDouble("lng");
//
//                            LatLng Provoder = new LatLng(provider_lati, provoder_longi);
//                            LatLng Customer = new LatLng(customer_lati, customer_longi);
                            //   googleMapBounds(Provoder, Customer);
                        }
                    }


//                    for (int i=0;i<resolve.length();i++){
//
//                        if (object1.has("lat") && !object1.isNull("lat") && object1.has("lng") && !object1.isNull("lng")) {
//                            lati = object1.getDouble("lat");
//                            longi = object1.getDouble("lng");
//                            userId = object1.getInt("usr_id");
//                            JSONObject provider=
//                            final LatLng provider = new LatLng(lati, longi);
////                            latLngHashMapSocket.put(userId,socketData);
////                            Log.e("Response", String.valueOf(latLngHashMap.size()));
////                            for (Object key : latLngHashMap.keySet()) {
////                                value = latLngHashMap.get(key);
////                                int a=Integer.valueOf(key.toString());
////                                if (userId == a){
////                                    if (socketMarker!=null){
////                                        socketMarker.remove();
////                                    }
////                                    value =socketData;
////                                    final LatLng finalValue = value;
////                                    runOnUiThread(new Runnable() {
////                                        @Override
////                                        public void run() {
////                                            Log.e("Response", "show truck");
////                                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(finalValue, DEFAULT_ZOOM));
////                                            socketMarker =mMap.addMarker(new MarkerOptions().position(finalValue).title("Provider").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_snow_truck)));
////
////                                        }
////                                    });
////
////                                    Log.e("data", "Key: " + key + " Value: " + finalValue);
////                                }
////                                //  Toast.makeText(getApplicationContext(), "Key: "+key+" Value: "+value, Toast.LENGTH_LONG).show();
////                            }
//                        } else {
//                            Toast.makeText(getApplicationContext(), "Driver not found", Toast.LENGTH_SHORT).show();
//                        }
//                    }


                } catch (Exception e) {
                    Log.e("Response", " " + e);
                }

            }

        }).on("status", new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                //   JSONObject position = (JSONObject) args[0];
                Log.e("Response", "status");


            }

        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                //  JSONObject position = (JSONObject) args[0];
                Log.e("Response", "disconnected");

            }


        });

    }

    private void googleMapBounds(LatLng provider, LatLng customer) {


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        mMap.getUiSettings().setTiltGesturesEnabled(true);
        mMap.getUiSettings().setRotateGesturesEnabled(true);
        if (marker != null) {
            marker.remove();
        }
        marker = mMap.addMarker(new MarkerOptions()
                .position(provider)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_marker))
        );
        if (marker1 != null) {
            marker1.remove();
        }
        marker1 = mMap.addMarker(new MarkerOptions()
                .position(customer)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.red_marker))
        );


        try {
            if (markers != null) {
                markers.clear();
            }
            markers.add(marker);
            markers.add(marker1);
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (Marker marker : markers) {
                builder.include(marker.getPosition());
            }
            LatLngBounds bounds = builder.build();
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 10);
            mMap.animateCamera(cu);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public LatLng getCenterCoordinate(LatLng customer, LatLng provider) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(customer);
        builder.include(provider);
        LatLngBounds bounds = builder.build();
        return bounds.getCenter();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        connection.unregisterNetworkChanges(this);
    }
}

