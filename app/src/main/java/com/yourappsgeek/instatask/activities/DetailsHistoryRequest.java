package com.yourappsgeek.instatask.activities;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.yourappsgeek.instatask.R;
import com.yourappsgeek.instatask.RetrofitClient.ApiClient;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class DetailsHistoryRequest extends AppCompatActivity
{

    TextView priceLane,providerName,reportIssue;
    MaterialRatingBar materialRatingBar;
    RoundedImageView beforeImage,afterImage;
    double latitude,longitude;
    float rate;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_history_request);

        priceLane=findViewById(R.id.textView16);
        providerName=findViewById(R.id.textView19);
        materialRatingBar=findViewById(R.id.ratingBar2);
        beforeImage=findViewById(R.id.roundedImageView);
        afterImage=findViewById(R.id.roundedImageView2);
        reportIssue=findViewById(R.id.textView20);


        priceLane.setText(getIntent().getStringExtra("price")+"$"+" / "+getIntent().getStringExtra("lane"));
        providerName.setText(getIntent().getStringExtra("provideName"));
        rate=getIntent().getFloatExtra("rating",5.0f);
        materialRatingBar.setRating(rate);
        String before= ApiClient.IMAGE_BASE_URL +getIntent().getStringExtra("before_img");
        String after=ApiClient.IMAGE_BASE_URL +getIntent().getStringExtra("after_img");



        if (before.equals(null) || before.equals("")){
            Picasso.with(getApplicationContext()).load(R.drawable.no_uploaded).into(beforeImage);
        }else {
            Picasso.with(getApplicationContext()).load(before).into(beforeImage);
        }

        if (after.equals(null) || after.equals("")){
            Picasso.with(getApplicationContext()).load(R.drawable.no_uploaded).into(afterImage);
        }else {
            Picasso.with(getApplicationContext()).load(after).into(afterImage);
        }


        reportIssue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DetailsHistoryRequest.this,HelpActivity.class));
            }
        });

        Log.e("images",before+" "+after);

        latitude=getIntent().getDoubleExtra("lat",0.0);
        longitude=getIntent().getDoubleExtra("long",0.0);

         MapView mapView = findViewById(R.id.mapView3);
         if (mapView != null) {
             // Initialise the MapView
             mapView.onCreate(null);
             // Set the map ready callback to receive the GoogleMap object
             mapView.getMapAsync(new OnMapReadyCallback()
             {
                 @Override
                 public void onMapReady(GoogleMap googleMap)
                 {
                     googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                     // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
                     try
                     {
                         MapsInitializer.initialize(DetailsHistoryRequest.this);
                         LatLng sydney = new LatLng(-34, 151);
                         googleMap.addMarker(new MarkerOptions().position(sydney).title("My Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_marker)));
                         googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                         googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                             @Override
                             public void onMapClick(LatLng latLng) {
                                 //do nothing, we want to suppress launching Google Maps
                             }
                         });
                     } catch (Exception e)
                     {
                         e.printStackTrace();
                     }
                 }
             });
         }
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.item_actionbar);

        TextView title = getSupportActionBar().getCustomView().getRootView().findViewById(R.id.tvTitle);

        title.setText(getString(R.string.title_activity_my_request));

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
