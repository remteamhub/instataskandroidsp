package com.yourappsgeek.instatask.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.yourappsgeek.instatask.Controller.ScheduleRequest;
import com.yourappsgeek.instatask.Interfaces.ScheduleJobCallbackListener;
import com.yourappsgeek.instatask.Services.Constants;
import com.yourappsgeek.instatask.R;
import com.yourappsgeek.instatask.ApiServices.APIService;
import com.yourappsgeek.instatask.RetrofitClient.ApiClient;
import com.yourappsgeek.instatask.SharedPreference.SaveSharedPreference;
import com.yourappsgeek.instatask.models.HistoryItem;
import com.yourappsgeek.instatask.models.UpdateStatus;
import com.yourappsgeek.instatask.social.AccountKitIntegrator;
import com.yourappsgeek.instatask.social.FacebookIntegrator;
import com.yourappsgeek.instatask.social.SocialCallback;
import com.yourappsgeek.instatask.social.TwitterIntegrator;
import com.yourappsgeek.instatask.utils.UiUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import im.delight.android.location.SimpleLocation;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.yourappsgeek.instatask.Controller.internetCheck.isConnected;


public class LoginActivity extends AppCompatActivity implements  ScheduleJobCallbackListener {

    FacebookIntegrator facebookIntegrator;
    AccountKitIntegrator integrator;
    CallbackManager callbackManager;
    @BindView(R.id.fblogin)
    LoginButton fblogin;
    @BindView(R.id.edtEmail)
    EditText editEmail;
    @BindView(R.id.edtPassword)
    EditText edtPassword;


    @BindView(R.id.wrapperEmail)
    TextInputLayout wrapperEmail;
    @BindView(R.id.wrapperPassword)
    TextInputLayout wrapperPassword;

    TwitterIntegrator twitterIntegrator;

    @BindView(R.id.twitterLogin)
    TwitterLoginButton twitterLogin;


    ProgressDialog progressDialog;
    private String TAG;
    private String phone1;
    private String image1;
    String license = null;
    private final String[] permissions = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION};
    @OnClick(R.id.btnSignIn)
    void signInClick() {

        if(!isConnected(LoginActivity.this));

        else {
            login();
        }
    }

    @OnClick(R.id.txtDontHaveAccount)
    void signUpClick() {
        startActivity(new Intent(LoginActivity.this, CustomerSignUpActivity.class));
        finish();
    }

    @OnClick(R.id.btnFacebook)
    void signInFb() {
        if(!isConnected(LoginActivity.this));

        else {
            doFbLogin();

        }
    }

    @OnClick(R.id.btnTwitter)
    void signInTwitter() {

        if(!isConnected(LoginActivity.this));

        else {
            doTwitterLogin();
        }

    }

    double latitude, longitude;

    SharedPreferences preferences;
    String fcm_token;
    SharedPreferences.Editor editor;
    private static final int REQUEST_CODE = 12345;
    private SimpleLocation location;
    TextView forgot;
    public LoginActivity() {
    }

    @SuppressLint({"WrongConstant", "CommitPrefEdits"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_login);
        if (ActivityCompat.checkSelfPermission(LoginActivity.this, permissions[0]) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(LoginActivity.this, permissions[1]) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(LoginActivity.this, permissions[2]) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(LoginActivity.this, permissions[3]) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LoginActivity.this, permissions, REQUEST_CODE);
        }

        try {
            if(Build.VERSION.SDK_INT >= 28) {
                @SuppressLint("WrongConstant") final PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNING_CERTIFICATES);
                final Signature[] signatures = packageInfo.signingInfo.getApkContentsSigners();
                final MessageDigest md = MessageDigest.getInstance("SHA");
                for (Signature signature : signatures) {
                    md.update(signature.toByteArray());
                    final String signatureBase64 = new String(Base64.encode(md.digest(), Base64.DEFAULT));
                    Log.d("Base64", signatureBase64);
                }
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        forgot=findViewById(R.id.txtForgotPassword);
        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),ForgotActivity.class));
                finish();
            }
        });
        location = new SimpleLocation(this);
        if (!location.hasLocationEnabled()) {
            // ask the user to enable location access
            SimpleLocation.openSettings(this);
        }
        latitude = location.getLatitude();
        longitude = location.getLongitude();


        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        Log.e("fcm_token",token);
                        SaveSharedPreference.setFCM_Token(getApplicationContext(),token);
                    }
                });


        fcm_token=SaveSharedPreference.getFCM_Token(getApplicationContext());
        progressDialog = UiUtils.getProgressDialog(LoginActivity.this);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        ButterKnife.bind(this);

        callbackManager = CallbackManager.Factory.create();
        editor = getSharedPreferences("SocialLogout", MODE_APPEND).edit();
        editor=preferences.edit();
        Constants.user_latitude = latitude;
        Constants.user_longitude = longitude;
        Log.e("location",latitude+ " "+ longitude+ "");

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE) {
            if (grantResults.length == 4 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED && grantResults[3] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(LoginActivity.this, "Permission granted!!!", Toast.LENGTH_LONG).show();
            }
            else {
                Toast.makeText(LoginActivity.this, "Necessary permissions not granted...", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    public void doTwitterLogin() {
        twitterIntegrator = new TwitterIntegrator(twitterLogin, new SocialCallback() {
            @Override
            public void onSocialLoginSuccess(String username, String first_name, String last_name, String email, String SocialId, String image, String userId, String accessToken, int type) {

                setSocialUser(SocialId, username,  email, first_name, last_name, image);

//                if (Constants.license_img) {
//                    startActivity(new Intent(LoginActivity.this,MainActivity.class));
//                    finish();
//                }else {
//                    SaveSharedPreference.setProfile_Image(getApplicationContext(),image);
//                }

            }

            @Override
            public void onSocialLoginFailed(String error) {
                Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
            }
        });
        twitterIntegrator.login();
    }

    public void doFbLogin() {
        facebookIntegrator = new FacebookIntegrator(fblogin, callbackManager, new SocialCallback() {
            @Override
            public void onSocialLoginSuccess(String username, String SocialId, String email, String first_name, String last_name, String image, String userId, String accessToken, int type) {
                progressDialog.show();
                setSocialUser(SocialId, username,  email, first_name, last_name, image);
            }

            @Override
            public void onSocialLoginFailed(String error) {
                Toast.makeText(LoginActivity.this, "Failed to authorize from facebook", Toast.LENGTH_SHORT).show();
            }
        });
        facebookIntegrator.login();
    }

    void setSocialUser(String social_id, final String username, String email, final String first_name, final String last_name, String image) {
        //Defining the user object as we need to pass it with the call
        Constants.social_id = social_id;
        Constants.userName = username;
        Constants.userEmail = email;
        Constants.userFirstName = first_name;
        Constants.userLastName = last_name;
       // Constants.profile_image = image;


       // Constants.social=true;

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("fulName", username);
        editor.putString("first_name", first_name);
        editor.putString("last_name", last_name);
        editor.putString("email", email);

        editor.putString("name",first_name+" "+last_name);

        editor.apply();
        fcm_token=SaveSharedPreference.getFCM_Token(getApplicationContext());
        ScheduleRequest scheduleRequest=new ScheduleRequest(getApplicationContext(),this,LoginActivity.this,this);
        scheduleRequest.SocialLogin(LoginActivity.this,social_id,username,fcm_token,email,first_name,last_name,image,latitude,longitude,"4");

//        Intent intent = new Intent(getApplicationContext(), LicenseUpload.class);
//        startActivity(intent);
//        finish();
        hideProgress();

    }

    public void Phone_Verification(final int user_id, final String fcm_token) {

        integrator = new AccountKitIntegrator(this, new SocialCallback() {

            @Override
            public void onSocialLoginSuccess(String username, String first_name, String last_name, String email, String SocialId, String image, String userId, String accessToken, int type) {
                Log.e("phonenumber", userId);

                Log.e("userDetail", userId);
                hideProgress();



                ScheduleRequest.UpdateProfile(LoginActivity.this,user_id,fcm_token,4,userId);
            }

            @Override
            public void onSocialLoginFailed(String error) {
                hideProgress();
                Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
            }
        });
        integrator.phoneLogin();
    }
//    void phone_update(final int user_id, final String fcm_token, String first_name, String last_name, int radius, final String phone) {
//
//        //Defining retrofit api service
//        APIService service = ApiClient.getClient().create(APIService.class);
//
//        //Defining the user object as we need to pass it with the call
//        Call<ResponseBody> call = service.updateProfile(user_id, fcm_token,first_name,last_name,radius,phone);
//
//        //calling the api
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
//                try {
//                    if (response.isSuccessful()) {
//                        JSONObject json = new JSONObject(response.body().string());
//                        Log.d("Response", json + "");
//                        int code = json.getInt("code");
//                        if (json.get("data") instanceof JSONObject && code == 200) {
//                            if (image1.equals("null") || image1.equals("")) {
//                                editor.putString("phone",phone1).commit();
//                                Intent intent = new Intent(getApplicationContext(), LicenseUpload.class);
//                                intent.putExtra("token", fcm_token);
//                                intent.putExtra("id", user_id);
//                                startActivity(intent);
//                                finish();
//                                hideProgress();
//                            } else {
//                                SaveSharedPreference.setLoggedIn(getApplicationContext(), true);
//                                editor.putString("phone",phone).commit();
//                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
//                                finish();
//                                hideProgress();
//                            }
//
//                        } else {
//                            hideProgress();
//                            Toast.makeText(getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();
//                        }
//
//                    } else {
//                        hideProgress();
//                        Toast.makeText(LoginActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                hideProgress();
//                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" +t.getMessage(),Toast.LENGTH_LONG).show();
//
//            }
//        });
//
//
//    }


    public void login() {
        Log.d(TAG, "Login");

        if (validate() == false) {
            return;
        }
        loginByServer();
    }

    private void loginByServer() {
        String email;
        final String password;
        try {
            progressDialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }


        fcm_token=SaveSharedPreference.getFCM_Token(getApplicationContext());
        email = editEmail.getText().toString();
        password = edtPassword.getText().toString();
        final APIService request = ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call = request.userSignIn(email, password, fcm_token);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {
                            JSONObject data = json.getJSONObject("data");
                            if (data.has("userProfile") && !data.isNull("userProfile") || data.has("ProviderDetails") && !data.isNull("ProviderDetails")) {

                                JSONObject userProfile = data.getJSONObject("userProfile");
                            JSONObject providerDetail = data.getJSONObject("ProviderDetails");

                            image1 = providerDetail.getString("license_img");
                            String first_name = userProfile.getString("first_name");
                            String last_name = userProfile.getString("last_name");
                            int id = userProfile.getInt("id");
                            String name = first_name + " " + last_name;
                            // int userId = userProfile.getInt("id");
                            int profileStatus = userProfile.getInt("status");
                            int jobStatus = providerDetail.getInt("job_status");
                            int approvalStatus = providerDetail.getInt("approval_status");
                            String email = userProfile.getString("email");
                            if (userProfile.has("avatar") && !userProfile.isNull("avatar")) {
                                try {
                                    String img=userProfile.getString("avatar");
                                    editor.putString("profile_image", img).apply();
                                }catch (Exception e){
                                    e.printStackTrace();
                                }

                            }
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putInt("profile_Status", profileStatus);
                            editor.putInt("job_Status", jobStatus);
                            editor.putInt("license_Status", approvalStatus);
                            editor.putInt("user_id", id);
                            editor.putString("fulName", name);
                            editor.putString("name", name);
                            editor.putString("email", email);
                            editor.apply();


                                if (userProfile.isNull("phone")) {
                                    Toast.makeText(getApplicationContext(), "Phone not Update. Try again!", Toast.LENGTH_SHORT).show();
                                } else if (providerDetail.isNull("license_img")) {
                                    startActivity(new Intent(getApplicationContext(), LicenseUpload.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                                    finish();

                                } else {
                                    String phone = userProfile.getString("phone");
                                    editor.putString("phone", phone).apply();
                                    SaveSharedPreference.setLoggedIn(getApplicationContext(), true);
                                    startActivity(new Intent(getApplicationContext(), MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                                    finish();

                                }
                            }
                            progressDialog.dismiss();
                        } else {

                            Toast.makeText(getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();
                        }


                        hideProgress();
                    }else {
                        hideProgress();
                        Toast.makeText(LoginActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("failure", t.getMessage());
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" +t.getMessage(),Toast.LENGTH_LONG).show();

            }
        });
    }

    void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (integrator != null)
            integrator.onActivityResult(requestCode, data);
        if (twitterLogin != null) twitterLogin.onActivityResult(requestCode, resultCode, data);
        if (this.callbackManager != null)
            this.callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public boolean validate() {
        boolean valid = true;

        String email = editEmail.getText().toString();
        String password = edtPassword.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            editEmail.setError("enter a valid email address");
            requestFocus(editEmail);
            valid = false;
        } else {
            editEmail.setError(null);
        }

        if (password.isEmpty()) {
            edtPassword.setError("Password is empty");
            requestFocus(edtPassword);
            valid = false;
        } else {
            edtPassword.setError(null);
        }

        return valid;

    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }



    @Override
    public void onFetchProgress(HistoryItem historyItem) {

    }

    @Override
    public void onUpdateStatusProgress(UpdateStatus status) {

    }

    @Override
    public void onFetchComplete() {

    }
}
