package com.yourappsgeek.instatask.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.sendbird.android.BaseChannel;
import com.sendbird.android.BaseMessage;
import com.sendbird.android.OpenChannel;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.UserMessage;
import com.yourappsgeek.instatask.ApiServices.APIService;
import com.yourappsgeek.instatask.Controller.DialogController;
import com.yourappsgeek.instatask.R;
import com.yourappsgeek.instatask.RetrofitClient.ApiClient;
import com.yourappsgeek.instatask.Services.Constants;
import com.yourappsgeek.instatask.models.MessageReceiveModel;
import com.yourappsgeek.instatask.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.yourappsgeek.instatask.Services.Constants.Provider_ID;
import static com.yourappsgeek.instatask.Services.Constants.User_ID;

public class ChatActivity extends AppCompatActivity {

    private ChatAdapter mChatAdapter;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private Button mSendButton;
    private EditText mMessageEditText;
    List<MessageReceiveModel> mesg;
    MessageReceiveModel messageReceiveModel;
    SwipeRefreshLayout pullToRefresh;
    int lastPage;
    public static boolean check12 = true;
    int Refreshcounter = 1;
    Date dt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        mSendButton = (Button) findViewById(R.id.button_chat_send);
        mMessageEditText = (EditText) findViewById(R.id.edittext_chat);
        pullToRefresh = (SwipeRefreshLayout) findViewById(R.id.pullToRefresh);
        mRecyclerView = (RecyclerView) findViewById(R.id.reycler_chat);
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setReverseLayout(true);
        mRecyclerView.setLayoutManager(mLayoutManager);

        Constants.mesg=null;
        mesg=new ArrayList<>();
        receiveMessage(User_ID, Provider_ID,Refreshcounter);

        mChatAdapter=new ChatAdapter(getApplicationContext(),mesg);
        mRecyclerView.setAdapter(mChatAdapter);
        mChatAdapter.notifyDataSetChanged();



        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                pullToRefresh.setRefreshing(false);
                Refreshcounter = Refreshcounter + 1;

                if (Refreshcounter<=lastPage) {
                    receivePreviousMessage(User_ID, Provider_ID, Refreshcounter);
                    mChatAdapter.notifyDataSetChanged();
                }else {
                    Toast.makeText(ChatActivity.this, "No more data to load!!",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });


        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                String mesg=mMessageEditText.getText().toString();
                sendMessage(User_ID,Provider_ID,mesg);
                mMessageEditText.setText("");
                try {
                    Constants.progressDialog.show();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });


    }

    public void receiveMessage(String to, String from,int page) {

        try {
            Constants.progressDialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }

        APIService request = ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call = request.getMessage(to, from,page);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {

                            JSONObject data=json.getJSONObject("data");
                            JSONArray message=data.getJSONArray("data");
                            lastPage=data.getInt("last_page");
                           // messageReceiveModel.setLast_page(LastPage);
                            mesg=new ArrayList<>();
                            for (int i=0; i<message.length();i++){



                                JSONObject object=message.getJSONObject(i);
                                String to=object.getString("to");
                                String from=object.getString("from");
                                String mess=object.getString("message");
                                Long time=object.getLong("msg_time");

                                java.util.Date d = new java.util.Date(time*1000L);
                                String itemDateStr = new SimpleDateFormat("hh:mm aa").format(d);


                                mesg.add(new MessageReceiveModel(mess,mess,itemDateStr,to,from));

                            }

                            mChatAdapter=new ChatAdapter(getApplicationContext(),mesg);
                            mRecyclerView.setAdapter(mChatAdapter);
                            mChatAdapter.notifyDataSetChanged();



                            //  setData(mesg);





                           // Toast.makeText(getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();

                        } else {
                            Toast.makeText(getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();

                        }
                    } else {
                        Toast.makeText(ChatActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }

                    hideProgress();
                } catch (JSONException e) {

                    e.printStackTrace();
                } catch (IOException e) {

                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }


    public void receivePreviousMessage(String to, String from,int page) {

        try {
            Constants.progressDialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }

        APIService request = ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call = request.getMessage(to, from,page);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {

                            JSONObject data=json.getJSONObject("data");
                            JSONArray message=data.getJSONArray("data");
                            lastPage=data.getInt("last_page");
                            // messageReceiveModel.setLast_page(LastPage);

                            for (int i=0; i<message.length();i++){



                                JSONObject object=message.getJSONObject(i);
                                String to=object.getString("to");
                                String from=object.getString("from");
                                String mess=object.getString("message");
                                Long time=object.getLong("msg_time");

                                java.util.Date d = new java.util.Date(time*1000L);
                                String itemDateStr = new SimpleDateFormat("hh:mm aa").format(d);

                                mesg.add(new MessageReceiveModel(mess,mess,itemDateStr,to,from));

                            }





                            //  setData(mesg);





                            // Toast.makeText(getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();

                        } else {
                            Toast.makeText(getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();

                        }
                    } else {
                        Toast.makeText(ChatActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }

                    hideProgress();
                } catch (JSONException e) {

                    e.printStackTrace();
                } catch (IOException e) {

                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }


    public void sendMessage(String to, String from, String message) {

        try {
            Constants.progressDialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }
        APIService request = ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call = request.sendMessage(to, from, message);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (code == 200) {

                            receiveMessage(User_ID, Provider_ID,1);



                        } else {
                            Toast.makeText(getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();

                        }
                    } else {
                        Toast.makeText(ChatActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }

                    hideProgress();
                } catch (JSONException e) {

                    e.printStackTrace();
                } catch (IOException e) {

                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (check12){
                check12=false;
            receiveMessage(User_ID, Provider_ID,Refreshcounter);}

        }
    };
    void hideProgress() {
        if (Constants.progressDialog != null && Constants.progressDialog.isShowing())
            Constants.progressDialog.dismiss();
    }

    @Override
    protected void onResume() {
        super.onResume();
            LocalBroadcastManager.getInstance(ChatActivity.this).registerReceiver(broadcastReceiver, new IntentFilter("CHAT"));

    }

    @Override
    protected void onPause() {
      //  SendBird.removeChannelHandler(CHANNEL_HANDLER_ID);

        super.onPause();
    }

    private class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private static final int VIEW_TYPE_MESSAGE_SENT = 1;
        private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;
        Context context;
        private List<MessageReceiveModel> mMessageList;

        public ChatAdapter(Context context, List<MessageReceiveModel> mMessageList) {
            this.context = context;
            this.mMessageList = mMessageList;

        }
        public void setData(ArrayList<MessageReceiveModel> newData) {
            mMessageList=newData;
            notifyDataSetChanged();
        }

        // Determines the appropriate ViewType according to the sender of the message.
        @Override
        public int getItemViewType(int position) {
            MessageReceiveModel message =mMessageList.get(position);

            if (message.getTo().equals(Provider_ID)) {
                // If the current user is the sender of the message
                return VIEW_TYPE_MESSAGE_RECEIVED;

            } else {
                // If some other user sent the message
                return VIEW_TYPE_MESSAGE_SENT;
            }
        }

        // Inflates the appropriate layout according to the ViewType.
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view;

            if (viewType == VIEW_TYPE_MESSAGE_SENT) {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_message_sent, parent, false);
                return new SentMessageHolder(view);
            } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_message_received, parent, false);
                return new ReceivedMessageHolder(view);
            }

            return null;
        }

        // Passes the message object to a ViewHolder so that the contents can be bound to UI.
        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            MessageReceiveModel message = mMessageList.get(position);

            switch (holder.getItemViewType()) {
                case VIEW_TYPE_MESSAGE_SENT:
                    ((SentMessageHolder) holder).SendmessageText.setText(message.getSendMesg());
                    ((SentMessageHolder) holder).timeText.setText(message.getCreated_at());
                    break;
                case VIEW_TYPE_MESSAGE_RECEIVED:
                    ((ReceivedMessageHolder) holder).ReceivemessageText.setText(message.getReceiveMesg());
                    ((ReceivedMessageHolder) holder).timeText.setText(message.getCreated_at());
            }

        }

        @Override
        public int getItemCount() {
            return mMessageList.size();
        }

        // Messages sent by me do not display a profile image or nickname.
        private class SentMessageHolder extends RecyclerView.ViewHolder {
            TextView SendmessageText, timeText;
            SentMessageHolder(View itemView) {
                super(itemView);
                SendmessageText = (TextView) itemView.findViewById(R.id.text_message_body);
                timeText = (TextView) itemView.findViewById(R.id.text_message_time);
            }

        }

        // Messages sent by others display a profile image and nickname.
        private class ReceivedMessageHolder extends RecyclerView.ViewHolder {
            TextView ReceivemessageText, timeText, nameText;
            ImageView profileImage;

            ReceivedMessageHolder(View itemView) {
                super(itemView);

                ReceivemessageText = (TextView) itemView.findViewById(R.id.text_message_body);
                timeText = (TextView) itemView.findViewById(R.id.text_message_time);
                //nameText = (TextView) itemView.findViewById(R.id.text_message_name);
//                profileImage = (ImageView) itemView.findViewById(R.id.image_message_profile);
            }
        }




    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        receiveMessage(User_ID, Provider_ID,Refreshcounter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(getApplicationContext(),MainActivity.class));
        finish();
    }
}
