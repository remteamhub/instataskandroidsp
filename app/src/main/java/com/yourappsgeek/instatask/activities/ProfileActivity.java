package com.yourappsgeek.instatask.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;
import com.yourappsgeek.instatask.CircleImageView;
import com.yourappsgeek.instatask.Controller.ScheduleRequest;
import com.yourappsgeek.instatask.Controller.internetCheck;
import com.yourappsgeek.instatask.Services.Constants;
import com.yourappsgeek.instatask.R;
import com.yourappsgeek.instatask.ApiServices.APIService;
import com.yourappsgeek.instatask.RetrofitClient.ApiClient;
import com.yourappsgeek.instatask.SharedPreference.SaveSharedPreference;
import com.yourappsgeek.instatask.config.ImageUri;
import com.yourappsgeek.instatask.utils.UiUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Objects;
import java.util.Random;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.yourappsgeek.instatask.Controller.internetCheck.isConnected;
import static com.yourappsgeek.instatask.Services.Constants.Social_images;
import static com.yourappsgeek.instatask.Services.Constants.realPath;
import static com.yourappsgeek.instatask.config.ImageUri.getImageUri;
import static com.yourappsgeek.instatask.config.ImageUri.getRealPathFromURI;

public class ProfileActivity extends AppCompatActivity {
    SeekBar seekBar;
    TextView textView, tvEditPassword, value_hint,tvContactNumber,tvEmailAddress,rate_value;
    EditText editName;
    ImageView editProfileImage;
    CircleImageView profileImage;
    Button saveChange;
    public static final int PICK_IMAGE = 1;
    Integer REQUEST_CAMERA = 1, SELECT_FILE = 0;
    Bitmap FixBitmap;
    ProgressDialog progressDialog;
    SharedPreferences preferences;
    Dialog alertDialog;
    String fcm_token, oldPassword;
    int userId, radius;
    float rating;
    SharedPreferences.Editor editor ;
    MaterialRatingBar materialRatingBar;
    SharedPreferences sharedPreferences;
    CircleImageView header_image;
    private int GALLERY = 1, CAMERA = 2;
    int progressValue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.item_actionbar);
        sharedPreferences=PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        fcm_token = SaveSharedPreference.getFCM_Token(getApplicationContext());
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();
        userId = preferences.getInt("user_id", 0);
        try {
            radius = preferences.getInt("radius", 0);
            rating= Float.parseFloat(preferences.getString("rate_value",""));
        }catch (Exception e){e.printStackTrace();}


        TextView title = getSupportActionBar().getCustomView().getRootView().findViewById(R.id.tvTitle);

        title.setText(getString(R.string.title_activity_profile));

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        }
        seekBar = findViewById(R.id.radius_seekBar);
        seekBar.setProgress(radius);
        seekBar.setEnabled(false);
        textView = findViewById(R.id.seek_bar_value);
        value_hint = findViewById(R.id.value_hint);
        rate_value=findViewById(R.id.textView);
        materialRatingBar=findViewById(R.id.rbRating);

        if (rating!=0) {
            materialRatingBar.setRating(rating);
            rate_value.setText(rating + "/5.0");
        }else {
            materialRatingBar.setRating(5.0f);
            rate_value.setText("5.0/5.0");
        }
        editName = findViewById(R.id.tvName);
        tvContactNumber=findViewById(R.id.tvContactNumber);
        tvEmailAddress=findViewById(R.id.tvEmailAddress);
        editName.setText(Constants.userFirstName + " " + Constants.userLastName);
        editName.setText(preferences.getString("name", "Jemmy Gogo"));
        tvContactNumber.setText(preferences.getString("phone", "123456789"));
        tvEmailAddress.setText(preferences.getString("email", "abc@email.com"));
        editProfileImage = findViewById(R.id.imgPicImage);
        profileImage = findViewById(R.id.imgProfil);
        saveChange = findViewById(R.id.save_change_btn);

        try {
            String pro_img=preferences.getString("profile_image","");
            String img = ApiClient.IMAGE_BASE_URL + pro_img;
            if (!pro_img.equals("")){
                Picasso.with(getApplicationContext()).load(img).into(profileImage);
                }else if(!Social_images.equals("")){
                Picasso.with(getApplicationContext()).load(Social_images).into(profileImage);

            }else {
                Picasso.with(getApplicationContext()).load(R.drawable.ic_profile_placeholder).into(profileImage);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        findViewById(R.id.tvEditPassword).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatepassDialog();
            }
        });
        seekBar();
        editProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getImage();
            }
        });
        saveChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String fullname;
                final String fName;
                final String lName;
                File file = null;

                fullname = editName.getText().toString();
                final int random = new Random().nextInt(1000000);
                int firstSpace = fullname.indexOf(" ");
                if (fullname.isEmpty()) {
                    Toast.makeText(ProfileActivity.this, "Name Field Can't be empty", Toast.LENGTH_SHORT).show();
                    return;
                } else if (firstSpace < 0) {
                    Toast.makeText(ProfileActivity.this, "Please Enter full name", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    fName = fullname.substring(0, firstSpace);
                    lName = fullname.substring(firstSpace).trim();
                    if (fName.equals(lName)) {
                        Toast.makeText(ProfileActivity.this, "Please Correct your name", Toast.LENGTH_SHORT).show();
                        return;
                    } else if (lName.length() < 2) {
                        Toast.makeText(ProfileActivity.this, "Please write your proper name", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                if(!isConnected(ProfileActivity.this));

                else {
                    showProgress();
                    try {
                        file = new File(realPath);

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    // Create a request body with file and image media type
                    RequestBody fileReqBody = null;
                    if (file != null) {
                        fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
                    }
                    // Create MultipartBody.Part using file request-body,file name and part name
                    MultipartBody.Part part=null;
                    if (fileReqBody!=null) {
                        part = MultipartBody.Part.createFormData("avatar_file", file.getName(), fileReqBody);
                    }

                    RequestBody first_name1 = null;
                    if (fName != null) {
                        first_name1 = RequestBody.create(MediaType.parse("text/plain"), fName);
                    }

                    RequestBody last_name1 = null;
                    if (lName != null) {
                        last_name1 = RequestBody.create(MediaType.parse("text/plain"), lName);
                    }

                    RequestBody fcm_token1 = null;
                    if (fcm_token != null) {
                        fcm_token1 = RequestBody.create(MediaType.parse("text/plain"), fcm_token);
                    }


                    updateProfile(userId, fcm_token1, seekBar.getProgress(), first_name1,last_name1,part);
                }

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(ProfileActivity.this,MainActivity.class));
                finish();
                return true;
        }
        int id = item.getItemId();

        if (id == R.id.nav_edit) {
            editProfileImage.setClickable(true);
            editProfileImage.setVisibility(View.VISIBLE);
            editName.setEnabled(true);
            seekBar.setEnabled(true);
            seekBar.setVisibility(View.VISIBLE);
            seekBar.setClickable(true);
            textView.setVisibility(View.VISIBLE);
            value_hint.setVisibility(View.VISIBLE);
            saveChange.setVisibility(View.VISIBLE);
        }
        return super.onOptionsItemSelected(item);
    }

    public void seekBar() {
        textView.setText(seekBar.getProgress() + " / " + seekBar.getMax() + " Miles");
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {


            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressValue = progress;
                textView.setText(progress + " / " + seekBar.getMax() + " Miles");

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                textView.setText(progressValue + " / " + seekBar.getMax() + " Miles");

            }
        });
    }

    public void getImage() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {"Select photo from gallery", "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
//        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        startActivityForResult(galleryIntent, GALLERY);

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    profileImage.setImageBitmap(bitmap);
                    Constants.imagePath = getImageUri(getApplicationContext(), bitmap);
                    Constants.realPath = getRealPathFromURI(getApplicationContext(),Constants.imagePath);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            Bitmap thumbnail = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");

//
            profileImage.setImageBitmap(thumbnail);
            Constants.imagePath = getImageUri(getApplicationContext(), thumbnail);
            Constants.realPath = getRealPathFromURI(getApplicationContext(),Constants.imagePath);
        }

    }


    void updatepassDialog() {
        alertDialog = new Dialog(ProfileActivity.this);
        alertDialog.setContentView(R.layout.update_pass);
        alertDialog.setCancelable(false);


        SharedPreferences preferences1 = PreferenceManager.getDefaultSharedPreferences(this);
        oldPassword = preferences1.getString("loginPassword", "");


        final LinearLayout oldLayout = alertDialog.findViewById(R.id.old_pass_layout);
        final LinearLayout newLayout = alertDialog.findViewById(R.id.new_pass_layout);
        final EditText oldPass = alertDialog.findViewById(R.id.old_pass);
        final EditText newPass = alertDialog.findViewById(R.id.new_pass);
        final EditText confirmPass = alertDialog.findViewById(R.id.confirm_pass);
        Button oldCancel = alertDialog.findViewById(R.id.old_cancel);
        Button oldSubmit = alertDialog.findViewById(R.id.old_submit);
        Button newCancel = alertDialog.findViewById(R.id.new_cancel);
        Button newSubmit = alertDialog.findViewById(R.id.new_submit);

        oldCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        oldSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (oldPass.length() < 5) {
                    Toast.makeText(ProfileActivity.this, "Password is not Match", Toast.LENGTH_SHORT).show();
                } else if (oldPass.getText().toString().equals(oldPassword)) {
                    newLayout.setVisibility(View.VISIBLE);
                    oldLayout.setVisibility(View.GONE);
                } else {
                    Toast.makeText(ProfileActivity.this, "Password is not Match", Toast.LENGTH_SHORT).show();
                }
            }
        });
        newCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newLayout.setVisibility(View.GONE);
                oldLayout.setVisibility(View.VISIBLE);
            }
        });
        newSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (newPass.length() < 7) {
                    Toast.makeText(ProfileActivity.this, "8 characters are required", Toast.LENGTH_SHORT).show();
                } else if (newPass.getText().toString().equals(confirmPass.getText().toString())) {

                    if(!isConnected(ProfileActivity.this));

                    else {
                        showProgress();
                        updatePassword(userId, fcm_token, oldPass.getText().toString(), newPass.getText().toString());
                    }

                } else {
                    Toast.makeText(ProfileActivity.this, "Confirm Password not match with new Password", Toast.LENGTH_SHORT).show();
                }
            }
        });
        alertDialog.show();
    }

    public void updatePassword(int id, String token, String oldP, final String newP) {
//        Gson gson = new GsonBuilder().setLenient().create();
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("http://instatask.trigoncab.com/api/provider/")
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .build();
        APIService request = ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call = request.updatePassword(id, token, oldP, newP);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject json = new JSONObject(response.body().string());
                    int code = json.getInt("code");
                    if (json.get("data") instanceof JSONObject && code == 200) {
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("loginPassword", newP);
                        editor.apply();
                        JSONObject data = json.getJSONObject("data");
                        Toast.makeText(ProfileActivity.this, data.getString("status"), Toast.LENGTH_SHORT).show();
                        alertDialog.dismiss();
                        Constants.dialog.dismiss();
                        hideProgress();

                    } else {
                        Toast.makeText(getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();
                        hideProgress();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    hideProgress();

                } catch (IOException e) {
                    e.printStackTrace();
                    hideProgress();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" +t.getMessage(),Toast.LENGTH_LONG).show();
                hideProgress();
            }
        });
    }

    void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    void showProgress() {
        progressDialog = UiUtils.getProgressDialog(ProfileActivity.this);
        progressDialog.show();
    }

    public void updateProfile(int id, RequestBody fb_token, int radius,RequestBody first_name,RequestBody last_name,MultipartBody.Part part) {

        APIService request = ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call = request.ImageUpdate(id, fb_token,  radius,first_name,last_name,part);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject json = new JSONObject(response.body().string());
                    Log.d("Response", json + "");
                    int code = json.getInt("code");
                    if (json.get("data") instanceof JSONObject && code == 200) {
                        JSONObject data = json.getJSONObject("data");
                        JSONObject userProfile = data.getJSONObject("userProfile");
                        JSONObject providerDetail = data.getJSONObject("ProviderDetails");
                        String first_name = userProfile.getString("first_name");
                        String last_name = userProfile.getString("last_name");
                        String phone=userProfile.getString("phone");
                        String email=userProfile.getString("email");
                        String id = userProfile.getInt("id") + "";
                        String name = first_name + " " + last_name;
                        realPath=null;
                       // Constants.profile_image=userProfile.getString("avatar");
                        if (userProfile.has("avatar")&& !userProfile.isNull("avatar")) {
                            editor.putString("profile_image", userProfile.getString("avatar")).apply();
                        }


                        Constants.userName = name;
                        Constants.userEmail = email;
                        Constants.userFirstName = first_name;
                        Constants.userLastName = last_name;


                        int userId = userProfile.getInt("id");
                        int profileStatus = userProfile.getInt("status");
                        int jobStatus = providerDetail.getInt("job_status");
                        int approvalStatus = providerDetail.getInt("approval_status");
                        int radius = providerDetail.getInt("radius");
                        editor.putInt("profile_Status", profileStatus).commit();
                        editor.putInt("job_Status", jobStatus).commit();
                        editor.putInt("license_Status", approvalStatus).commit();
                        editor.putInt("user_id", userId).commit();
                        editor.putInt("radius", radius).commit();
//                        editor.putString("fulName", name).commit();
                        editor.putString("phone",phone).commit();
                        editor.putString("email",email).commit();
                        editProfileImage.setClickable(false);
                        editProfileImage.setVisibility(View.GONE);
                        seekBar.setEnabled(false);
                        seekBar.setProgress(radius);
                        seekBar.setClickable(false);
                        editName.setEnabled(false);
                        saveChange.setVisibility(View.GONE);
                        Toast.makeText(ProfileActivity.this, data.getString("status"), Toast.LENGTH_SHORT).show();
                        hideProgress();
                    } else {
                        Toast.makeText(getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();
                        hideProgress();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    hideProgress();

                } catch (IOException e) {
                    e.printStackTrace();
                    hideProgress();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" +t.getMessage(),Toast.LENGTH_LONG).show();
                hideProgress();
            }
        });
    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(ProfileActivity.this,MainActivity.class));
        finish();
        super.onBackPressed();
    }
}
