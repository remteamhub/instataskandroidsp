package com.yourappsgeek.instatask.activities;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.yourappsgeek.instatask.ApiServices.APIService;
import com.yourappsgeek.instatask.R;
import com.yourappsgeek.instatask.RetrofitClient.ApiClient;
import com.yourappsgeek.instatask.Services.Constants;
import com.yourappsgeek.instatask.utils.CountryList;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BankInfoActivity extends AppCompatActivity
{
    ScrollView snake_bar;
    EditText bank_name,swift_code,iban,account_number,amount;
    AutoCompleteTextView country;
    Button withdraw_btn;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_info);
        initView();
        GetBankInfo(String.valueOf(Constants.userId));


//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
//                R.layout.custom_list_item, R.id.text_view_list_item, CountryList.getCountry());
//        country.setAdapter(adapter);

    }

    private void initView(){

        bank_name=findViewById(R.id.textView172);
        swift_code=findViewById(R.id.textView173);
        iban=findViewById(R.id.textView175);
        account_number=findViewById(R.id.textView174);
        amount=findViewById(R.id.textView176);
        country = findViewById(R.id.textView17);
        snake_bar=findViewById(R.id.scroll);
        withdraw_btn=findViewById(R.id.btnSubmit);
        WithDrawBtn();
    }

    private void WithDrawBtn(){
        withdraw_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Validation();
            }
        });
    }

    private void Validation(){



        if(TextUtils.isEmpty(bank_name.getText().toString())) {
            bank_name.setError("plz enter bank name!");
        }else if (TextUtils.isEmpty(swift_code.getText().toString())){
            swift_code.setError("plz enter Swift Code!");
        }else if (TextUtils.isEmpty(iban.getText().toString())){
            iban.setError("plz enter IBAN!");
        }else if (TextUtils.isEmpty(account_number.getText().toString())){
            account_number.setError("plz enter Account Number!");
        }else if (TextUtils.isEmpty(amount.getText().toString())){
            amount.setError("plz enter Amount!");
        }else if (TextUtils.isEmpty(country.getText().toString())){
            country.setError("plz enter your Country!");
        }else {
            String bankName=bank_name.getText().toString();
            String SwiftCode=swift_code.getText().toString();
            String IBAN=iban.getText().toString();
            String AccountNumber=account_number.getText().toString();
            String Ammount=amount.getText().toString();
            String Country=country.getText().toString();
            BankInfoApi(String.valueOf(Constants.userId),bankName,SwiftCode,IBAN,AccountNumber,Ammount,Country);

        }

    }


    private void BankInfoApi(String user_id,String bankname,String swiftcode,String iban,String account_no,String amount,String country){

        APIService service= ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call=service.bankinfo(user_id,bankname,swiftcode,iban,account_no,country,amount);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                if (response.isSuccessful()){

                    JSONObject json = new JSONObject(response.body().string());
                    Log.d("Response", json + "");
                    int code = json.getInt("code");
                    if (json.get("data") instanceof JSONObject && code == 200) {
                        Snackbar.make(snake_bar,json.getString("error_msg"), Snackbar.LENGTH_LONG).show();
                    }else {
                        Snackbar.make(snake_bar, json.getString("error_msg"), Snackbar.LENGTH_LONG).show();
                    }

                     //  int code = json.getInt("code");

                }else {
                    Snackbar.make(snake_bar,response.message(),Snackbar.LENGTH_LONG).show();
                }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Snackbar.make(snake_bar,t.getMessage(),Snackbar.LENGTH_LONG).show();
            }
        });

    }

    public void GetBankInfo(String user_id) {

        APIService request = ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call = request.getbankinfo(user_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject json = new JSONObject(response.body().string());
                    Log.d("Response", json + "");
                    int code = json.getInt("code");
                    if (json.get("data") instanceof JSONObject && code == 200) {

                        JSONObject data=json.getJSONObject("data");
                        bank_name.setText(data.getString("bank_name"));
                        swift_code.setText(data.getString("swift_code"));
                        iban.setText(data.getString("IBAN"));
                        account_number.setText(data.getString("account_no"));
                        country.setText(data.getString("country"));

                    } else {
                        Toast.makeText(getApplicationContext(), json.getString("error_msg"), Toast.LENGTH_LONG).show();

                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                } catch (IOException e) {

                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Toast.makeText(getApplicationContext(), "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
