package com.yourappsgeek.instatask.activities;

import android.os.Bundle;

import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.yourappsgeek.instatask.Controller.ScheduleRequest;
import com.yourappsgeek.instatask.Controller.internetCheck;
import com.yourappsgeek.instatask.R;
import com.yourappsgeek.instatask.Services.Constants;

import static com.yourappsgeek.instatask.Controller.internetCheck.isConnected;

public class HelpActivity extends AppCompatActivity {

    EditText edtIssueKind, edtIssueType, edtDescription;
    Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        edtIssueKind = findViewById(R.id.edtIssueKind);
        edtIssueType = findViewById(R.id.edtIssueType);
        edtDescription = findViewById(R.id.edtDescription);
        btnSubmit = findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!isConnected(HelpActivity.this));

                else {

                    String kind = edtIssueKind.getText().toString();
                    String type = edtIssueType.getText().toString();
                    String discription = edtDescription.getText().toString();
                    int text_length = discription.length();
                    if (kind.matches("")) {
                        Toast.makeText(HelpActivity.this, "Please add issue kind!", Toast.LENGTH_SHORT).show();
                        return;
                    } else if (type.matches("")) {
                        Toast.makeText(HelpActivity.this, "Please add issue type!", Toast.LENGTH_SHORT).show();
                        return;
                    } else if (discription.matches("")) {
                        Toast.makeText(HelpActivity.this, "Please add issue discription!", Toast.LENGTH_SHORT).show();
                        return;
                    } else if (text_length < 20) {
                        Toast.makeText(HelpActivity.this, "Minimum 20 character!", Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        ScheduleRequest.Help(HelpActivity.this, Constants.userId, "Provider", kind, type, discription, "Satisfied");
                    }
                }
            }

        });


        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.item_actionbar);

        TextView title = getSupportActionBar().getCustomView().getRootView().findViewById(R.id.tvTitle);

        title.setText(getString(R.string.title_activity_help));

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
