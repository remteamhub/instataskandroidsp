package com.yourappsgeek.instatask.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yourappsgeek.instatask.Controller.internetCheck;
import com.yourappsgeek.instatask.Interfaces.buttoncallback_listener;
import com.yourappsgeek.instatask.R;
import com.yourappsgeek.instatask.adapters.HistoryRequestAdapter;
import com.yourappsgeek.instatask.models.HistoryItem;

import java.util.ArrayList;

import static com.yourappsgeek.instatask.Controller.internetCheck.isConnected;

public class PaymentActivity extends AppCompatActivity
{
RecyclerView recyclerView ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        }
        recyclerView = findViewById(R.id.rvPrevious);

        if(!isConnected(PaymentActivity.this));

        else {


            ArrayList<HistoryItem> list = new ArrayList<>();
//        list.add(new HistoryItem());
//        list.add(new HistoryItem());
            HistoryRequestAdapter adapter = new HistoryRequestAdapter(list, this, R.layout.payment_list_item, new buttoncallback_listener() {
                @Override
                public void onClick(View view, int possition) {

                }
            });

            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(adapter);
            recyclerView.hasFixedSize();
            findViewById(R.id.btnSubmit).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(PaymentActivity.this, BankInfoActivity.class));
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
