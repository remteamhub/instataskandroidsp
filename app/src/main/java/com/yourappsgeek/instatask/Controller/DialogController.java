package com.yourappsgeek.instatask.Controller;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatEditText;

import com.yourappsgeek.instatask.ApiServices.APIService;
import com.yourappsgeek.instatask.R;
import com.yourappsgeek.instatask.RetrofitClient.ApiClient;
import com.yourappsgeek.instatask.Services.Constants;
import com.yourappsgeek.instatask.SharedPreference.SaveSharedPreference;
import com.yourappsgeek.instatask.activities.LoginActivity;
import com.yourappsgeek.instatask.activities.ProfileActivity;
import com.yourappsgeek.instatask.utils.UiUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DialogController {
    ProgressDialog progressDialog;
     Activity activity;
    RelativeLayout editjob;
    SharedPreferences preferences;

    public DialogController(Activity activity) {
        this.activity = activity;
        progressDialog = UiUtils.getProgressDialog(activity);
        preferences= PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
    }
    public static void showDialog(Activity activity){
        Constants.dialog = new Dialog(activity);
        Constants.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Constants.dialog.setCancelable(false);
        Constants.dialog.setContentView(R.layout.edit_job_request_dialog);
        Window window = Constants.dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
//        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
//        text.setText(msg);
        Button cancel =Constants.dialog.findViewById(R.id.button2);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScheduleRequest.editRequestStatus(activity,Constants.Job_Id,0);


            }
        });

        Button confirm =Constants.dialog.findViewById(R.id.button7);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScheduleRequest.editRequestStatus(activity,Constants.Job_Id,1);

            }
        });

        Constants.dialog.show();
    }


    public static void changepass_dialog(Activity activity,int id){
        String fcm_token = SaveSharedPreference.getFCM_Token(activity.getApplicationContext());
        Constants.dialog = new Dialog(activity);
        Constants.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Constants.dialog.setCancelable(false);
        Constants.dialog.setContentView(R.layout.change_password);
        Window window = Constants.dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
//        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
//        text.setText(msg);
        Button button =Constants.dialog.findViewById(R.id.new_submit);
        AppCompatEditText new_pass=Constants.dialog.findViewById(R.id.new_pass);
        AppCompatEditText confirm_pass=Constants.dialog.findViewById(R.id.confirm_pass);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(new_pass.getText().toString())) {
                    new_pass.setError("Enter New Password!");
                }else if (TextUtils.isEmpty(confirm_pass.getText().toString())){
                    confirm_pass.setError("Enter Confirm Password!");
                } else if (!new_pass.getText().toString().matches(confirm_pass.getText().toString())){
                    confirm_pass.setError("Password not match!");
                }else {
                    String c_pass=confirm_pass.getText().toString();
                    updatePassword(activity,id,fcm_token,"1",c_pass);
                }

            }
        });

        Constants.dialog.show();
    }


    public static void updatePassword(Activity activity, int id, String token, String old_password,String password) {
//        Gson gson = new GsonBuilder().setLenient().create();
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("http://instatask.trigoncab.com/api/provider/")
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .build();
        APIService request = ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call = request.ChangePassword(id, token,old_password, password);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject json = new JSONObject(response.body().string());
                    int code = json.getInt("code");
                    if (json.get("data") instanceof JSONObject && code == 200) {
//                        SharedPreferences.Editor editor = preferences.edit();
//                        editor.putString("loginPassword", newP);
//                        editor.apply();
                        JSONObject data = json.getJSONObject("data");
                        Toast.makeText(activity, data.getString("status"), Toast.LENGTH_SHORT).show();
                        activity.startActivity(new Intent(activity, LoginActivity.class));
                        activity.finish();
                        Constants.dialog.dismiss();
                        hideProgress();

                    } else {
                        Toast.makeText(activity, json.getString("error_msg"), Toast.LENGTH_LONG).show();
                        hideProgress();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    hideProgress();

                } catch (IOException e) {
                    e.printStackTrace();
                    hideProgress();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(activity, "Internet Connection is not stable" + "\n" +t.getMessage(),Toast.LENGTH_LONG).show();
                hideProgress();
            }
        });
    }


  static void hideProgress() {
        if (Constants.progressDialog != null && Constants.progressDialog.isShowing())
            Constants.progressDialog.dismiss();
    }

}
