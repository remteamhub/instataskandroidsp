package com.yourappsgeek.instatask.Controller;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.yourappsgeek.instatask.ApiServices.APIService;
import com.yourappsgeek.instatask.Interfaces.ScheduleJobCallbackListener;
import com.yourappsgeek.instatask.RetrofitClient.ApiClient;
import com.yourappsgeek.instatask.Services.Constants;
import com.yourappsgeek.instatask.SharedPreference.SaveSharedPreference;
import com.yourappsgeek.instatask.activities.LicenseUpload;
import com.yourappsgeek.instatask.activities.LoginActivity;
import com.yourappsgeek.instatask.activities.MainActivity;
import com.yourappsgeek.instatask.models.HistoryItem;
import com.yourappsgeek.instatask.social.AccountKitIntegrator;
import com.yourappsgeek.instatask.utils.UiUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScheduleRequest extends Activity {

    String userId = String.valueOf(Constants.userId);
    @SuppressLint("StaticFieldLeak")
    private Context context;
    Activity activity;
    ScheduleJobCallbackListener jobCallbackListener;
    AccountKitIntegrator integrator;
    CallbackManager callbackManager;
    LoginActivity loginActivity;

    public ScheduleRequest(Context context, Activity activity, ScheduleJobCallbackListener jobCallbackListener, LoginActivity loginActivity) {
        this.context = context;
        this.jobCallbackListener = jobCallbackListener;
        this.activity = activity;
        this.loginActivity = loginActivity;

    }

    public ScheduleRequest(Context context, Activity activity, ScheduleJobCallbackListener jobCallbackListener, Activity parent) {
        this.context = context;
        this.jobCallbackListener = jobCallbackListener;
        this.activity = activity;


    }



    public void loadScheduleJob() {

        APIService request = ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call = request.checkPendingJob(userId, "1");


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {
                    try {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {
                            JSONObject data = json.getJSONObject("data");
                            JSONArray job = data.getJSONArray("job");

                            for (int i = 0; i < job.length(); i++) {

                                JSONObject object = job.getJSONObject(i);
                                JSONObject user = object.getJSONObject("user");
                                HistoryItem historyItem = new HistoryItem.Schedule().
                                        setJob_id(object.getInt("id")).setAddress(object.getString("location_address")).
                                        setLatitude(object.getDouble("lat")).setLongitude(object.getDouble("lng")).
                                        setPrice(object.getString("service_price")).setDate_time(user.getString("created_at")).
                                        setCustomer_name(user.getString("first_name") + " " + user.get("last_name")).setService_type(object.getString("service_name")).setRating(user.getString("rating")).build();

                                jobCallbackListener.onFetchProgress(historyItem);


                            }


                        } else {
                            Toast.makeText(context, json.getInt("msg"), Toast.LENGTH_SHORT).show();

                        }
                        jobCallbackListener.onFetchComplete();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();

                jobCallbackListener.onFetchComplete();
            }
        });

    }


    public void JobsHistory() {

        APIService request = ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call = request.checkPreviousJob(userId, "1");


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {
                    try {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONArray && code == 200) {
                            JSONArray data = json.getJSONArray("data");
//                            JSONArray job=data.getJSONArray("job");

                            for (int i = 0; i < data.length(); i++) {

                                JSONObject object = data.getJSONObject(i);

                                HistoryItem historyItem = null;
                                if (object.has("user") && !object.isNull("user") && object.has("provider") && !object.isNull("provider")) {
                                    JSONObject provider = object.getJSONObject("provider");
                                    JSONObject user = object.getJSONObject("user");
                                    historyItem = new HistoryItem.Schedule().
                                            setJob_id(object.getInt("id")).setAddress(object.getString("location_address")).
                                            setLatitude(object.getDouble("lat")).setLongitude(object.getDouble("lng")).
                                            setPrice(object.getString("service_price")).setDate_time(object.getString("job_schedual_time")).
                                            setCustomer_name(user.getString("first_name") + " " + user.get("last_name")).setService_type(object.getString("service_name")).setRating(user.getString("rating")).setBeforeImg(object.getString("current_situation_img")).setAfterImg(object.getString("after_work_img")).build();

                                    jobCallbackListener.onFetchProgress(historyItem);
                                }

                            }
                        } else {
                            Toast.makeText(context, json.getInt("msg"), Toast.LENGTH_SHORT).show();

                        }
                        jobCallbackListener.onFetchComplete();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
                jobCallbackListener.onFetchComplete();

            }
        });

    }


    public static void editRequestStatus(final Activity context, int job_id, int status) {

        APIService request = ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call = request.editRequestStatus(job_id, status);
        Constants.progressDialog = UiUtils.getProgressDialog(context);
        try{
            Constants.progressDialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {
                    try {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (code == 200) {
//                            JSONArray data=json.getJSONArray("data");
//                            JSONArray job=data.getJSONArray("job");
                            Constants.dialog.dismiss();
                            Toast.makeText(context, json.getString("msg"), Toast.LENGTH_SHORT).show();


                        } else {
                            Toast.makeText(context, json.getString("msg"), Toast.LENGTH_SHORT).show();

                        }

                        Constants.dialog.dismiss();
                        hideProgress();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                Toast.makeText(context, "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

    }


    public static void Help(final Activity activity, int customer_id, String user_type, String issue_name, String issue_type, String issue_dis, String status) {


         Constants.progressDialog = UiUtils.getProgressDialog(activity);

        //Defining retrofit api service
        APIService service = ApiClient.getClient().create(APIService.class);

        //Defining the user object as we need to pass it with the call
        Call<ResponseBody> call = service.help(customer_id, user_type, issue_name, issue_type, issue_dis, status);

        //calling the api
        try {
            Constants.progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {

                            Toast.makeText(activity, json.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(activity, "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
                hideProgress();
            }
        });

    }


    public void SocialLogin(final Activity context, String social_id, final String username, final String fcm_token, String email, final String first_name, final String last_name, final String image, double lati, double longi, String radius) {

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        Constants.progressDialog = UiUtils.getProgressDialog(activity);
        try{
            Constants.progressDialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }
        final APIService request = ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call = request.SocialLogin(social_id, username, fcm_token, email, first_name, last_name, image, lati, longi, radius);
        //calling the api
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {
                            JSONObject data = json.getJSONObject("data");
                            JSONObject userProfile = data.getJSONObject("userProfile");
                            JSONObject providerDetail = data.getJSONObject("ProviderDetails");
                            SharedPreferences.Editor editor = preferences.edit();
                            int id = userProfile.getInt("id");
                            editor.putInt("user_id", id).apply();
                            editor.putString("Social_images",image).apply();

                            if (userProfile.has("phone") || providerDetail.has("license_img")) {

                                if (userProfile.isNull("phone")) {
                                    //loginActivity=new LoginActivity();

                                    loginActivity.Phone_Verification(id, fcm_token);
                                    Log.e("phone", "phone");
                                } else if (providerDetail.isNull("license_img")) {
                                    Log.e("phone", "image");
                                    context.startActivity(new Intent(context, LicenseUpload.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else {
                                    String phone = userProfile.getString("phone");
                                    editor.putString("phone", phone).commit();
                                    SaveSharedPreference.setLoggedIn(context, true);
                                    context.startActivity(new Intent(context, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                                }
                            }


                        } else {
                            Log.d("fbResponce", "" + json.getString("error_msg"));
                            Toast.makeText(context, json.getString("error_msg"), Toast.LENGTH_LONG).show();
                        }

                    } else {

                        Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
//                    String image = (json.getData().getProviderDetails().getLicenseImg() != null) ? json.getData().getProviderDetails().getLicenseImg().toString() : "";
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("fbResponce", "" + t);
                hideProgress();
                Toast.makeText(context, "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }


    public static void UpdateProfile(Activity activity, final int user_id, String fcm_token, int radius, final String phone) {

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(activity);
        final SharedPreferences.Editor editor=preferences.edit();
        Constants.progressDialog = UiUtils.getProgressDialog(activity);
        try{
            Constants.progressDialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }

        APIService service = ApiClient.getClient().create(APIService.class);

        //Defining the user object as we need to pass it with the call
        Call<ResponseBody> call = service.updateProfile(user_id, fcm_token, radius, phone);

        //calling the api
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {

                            JSONObject data = json.getJSONObject("data");
                            if (data.has("userProfile") && !data.isNull("userProfile") || data.has("ProviderDetails") && !data.isNull("ProviderDetails")) {

                                JSONObject userProfile = data.getJSONObject("userProfile");
                                JSONObject providerDetails = data.getJSONObject("ProviderDetails");

                                if (userProfile.isNull("phone")) {
                                    Toast.makeText(activity, "Phone not Update. Try again!", Toast.LENGTH_SHORT).show();
                                    activity.startActivity(new Intent(activity, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

                                } else if (providerDetails.isNull("license_img")) {
                                    activity.startActivity(new Intent(activity, LicenseUpload.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

                                } else {

                                        String phone = userProfile.getString("phone");
                                        editor.putString("phone", phone).apply();
                                        SaveSharedPreference.setLoggedIn(activity, true);
                                    activity.startActivity(new Intent(activity, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

                                }
                            }



                        } else {

                            Toast.makeText(activity, json.getString("error_msg"), Toast.LENGTH_LONG).show();
                        }

                    } else {

                        Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                    }

                    hideProgress();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                Toast.makeText(activity, "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }


    public static void LicenseImageUpload(final Activity context, final int user_id, RequestBody fcm_token, MultipartBody.Part LicenseImage) {

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor=preferences.edit();

        try{
            Constants.progressDialog = UiUtils.getProgressDialog(context);
            Constants.progressDialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }

        APIService service = ApiClient.getClient().create(APIService.class);

        //Defining the user object as we need to pass it with the call
        Call<ResponseBody> call = service.userProfileUpdateLicense(user_id, fcm_token, LicenseImage);

        //calling the api
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {

                            JSONObject data = json.getJSONObject("data");
                            if (data.has("userProfile") && !data.isNull("userProfile") || data.has("ProviderDetails") && !data.isNull("ProviderDetails")) {

                                JSONObject userProfile = data.getJSONObject("userProfile");
                                JSONObject providerDetails = data.getJSONObject("ProviderDetails");

                                if (userProfile.isNull("phone")) {
                                    Toast.makeText(context, "Phone not Update. Try again!", Toast.LENGTH_SHORT).show();
                                    context.startActivity(new Intent(context, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                                    context.finish();

                                } else if (providerDetails.isNull("license_img")) {
                                    context.startActivity(new Intent(context, LicenseUpload.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                                    context.finish();

                                } else {
                                    String phone = userProfile.getString("phone");
                                    editor.putString("phone", phone).apply();
                                    SaveSharedPreference.setLoggedIn(context, true);
                                    context.startActivity(new Intent(context, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                                    context.finish();

                                }
                            }

//                            startActivity(new Intent(context, MainActivity.class));
//                            finish();


                        } else {

                            Toast.makeText(context, json.getString("error_msg"), Toast.LENGTH_LONG).show();
                        }

                    } else {

                        Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                    }

                    hideProgress();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                Toast.makeText(context, "Internet Connection is not stable" + "\n" + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }


    public  void Register(final Context context, String full_name, String first_name, String last_name, String email,String phone, int radius, String password, String fb_token) {
        final SharedPreferences preferences=PreferenceManager.getDefaultSharedPreferences(context);
        final APIService request = ApiClient.getClient().create(APIService.class);
        Constants.progressDialog = UiUtils.getProgressDialog(activity);
        try{
            Constants.progressDialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }
        Call<ResponseBody> call = request.createUser(full_name, first_name, last_name, email,phone,  radius, password, fb_token);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response55", json + "");
                        JSONObject error_msg;
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {
                            JSONObject data = json.getJSONObject("data");
                            if (data.has("userProfile") && !data.isNull("userProfile") || data.has("ProviderDetails") && !data.isNull("ProviderDetails")) {
                                JSONObject userProfile = data.getJSONObject("userProfile");
                                JSONObject providerDetail = data.getJSONObject("ProviderDetails");
                                SharedPreferences.Editor editor = preferences.edit();
                                int id = userProfile.getInt("id");
                                editor.putInt("user_id", id).apply();
                                if (userProfile.isNull("phone")) {
                                   String fcm_token=SaveSharedPreference.getFCM_Token(getApplicationContext());

                                    loginActivity.Phone_Verification(id, fcm_token);

                                } else if (providerDetail.isNull("license_img")) {
                                    context.startActivity(new Intent(context, LicenseUpload.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                                } else {
                                    String phone = userProfile.getString("phone");
                                    editor.putString("phone", phone).apply();
                                    SaveSharedPreference.setLoggedIn(context, true);
                                    context.startActivity(new Intent(context, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

                                }
                            }
                        } else {
                            error_msg = json.getJSONObject("error_msg");
                            if (error_msg.has("phone") && !error_msg.isNull("phone") && error_msg.has("email") && !error_msg.isNull("email") && error_msg.has("fcm_token") && !error_msg.isNull("fcm_token")) {
                                JSONArray phone_array = error_msg.getJSONArray("phone");
                                JSONArray email = error_msg.getJSONArray("email");
                                JSONArray fcm_token = error_msg.getJSONArray("fcm_token");
                                String phone = phone_array.getString(0);
                                String _email = email.getString(0);
                                String _fcm_token = fcm_token.getString(0);
                                Toast.makeText(context,phone+"\n"+_email+"\n"+_fcm_token, Toast.LENGTH_LONG).show();
                            } else if(error_msg.has("phone") && !error_msg.isNull("phone") && error_msg.has("email") && !error_msg.isNull("email")){
                                JSONArray phone_array = error_msg.getJSONArray("phone");
                                JSONArray email = error_msg.getJSONArray("email");
                                String phone = phone_array.getString(0);
                                String _email = email.getString(0);
                                Toast.makeText(context,phone+"\n"+_email, Toast.LENGTH_LONG).show();

                            }else if (error_msg.has("phone") && !error_msg.isNull("phone")) {
                                JSONArray phone_array = error_msg.getJSONArray("phone");
                                String phone = phone_array.getString(0);
                                Toast.makeText(context,phone, Toast.LENGTH_LONG).show();
                            }else if (error_msg.has("email") && !error_msg.isNull("email")){
                                JSONArray email = error_msg.getJSONArray("email");
                                String _email = email.getString(0);
                                Toast.makeText(context,_email, Toast.LENGTH_LONG).show();
                            }else if (error_msg.has("fcm_token") && !error_msg.isNull("fcm_token")){
                                JSONArray fcm_token = error_msg.getJSONArray("fcm_token");
                                String _fcm_token = fcm_token.getString(0);
                                Toast.makeText(context,_fcm_token, Toast.LENGTH_LONG).show();
                            }
                        }
                    } else {
                        Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                    }
                hideProgress();
                } catch (
                        JSONException e) {
                    e.printStackTrace();
                    //    hideProgress();

                } catch (IOException e) {
                    e.printStackTrace();
                    //   hideProgress();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Error", t.getMessage());
                hideProgress();
                Toast.makeText(context, "Internet Connection is not stable" + "\n" +t.getMessage(),Toast.LENGTH_LONG).show();

            }
        });

    }










  private static void hideProgress() {

        if (Constants.progressDialog != null && Constants.progressDialog.isShowing())
            Constants.progressDialog.dismiss();
    }

}
