package com.yourappsgeek.instatask.Controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.yourappsgeek.instatask.ApiServices.APIService;
import com.yourappsgeek.instatask.Interfaces.ScheduleJobCallbackListener;
import com.yourappsgeek.instatask.R;
import com.yourappsgeek.instatask.RetrofitClient.ApiClient;
import com.yourappsgeek.instatask.Services.Constants;
import com.yourappsgeek.instatask.activities.MainActivity;
import com.yourappsgeek.instatask.models.UpdateStatus;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class updateStatuscController {


    Activity activity;
    ScheduleJobCallbackListener jobCallbackListener;

    public updateStatuscController(Activity activity, ScheduleJobCallbackListener jobCallbackListener) {
        this.activity= activity;
        this.jobCallbackListener = jobCallbackListener;
    }

    public void updateStatus(final int jobStatus, final int job_ID, MultipartBody.Part image) {
        Constants.progressDialog.show();
        APIService request = ApiClient.getClient().create(APIService.class);
        Call<ResponseBody> call = request.updateStatus(jobStatus, job_ID, image);
        Log.e("calldata", String.valueOf(image));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject json = new JSONObject(response.body().string());
                        Log.d("Response", json + "");
                        int code = json.getInt("code");
                        if (json.get("data") instanceof JSONObject && code == 200) {
                            JSONObject data=json.getJSONObject("data");
                            int id=data.getInt("id");
                            UpdateStatus status=new UpdateStatus.UP_Status().setJobid(id).build();
                            activity.startActivity(new Intent(activity,MainActivity.class));
                            activity.finish();
                            jobCallbackListener.onUpdateStatusProgress(status);
                        } else {
                            Toast.makeText(activity, json.getString("msg"), Toast.LENGTH_LONG).show();
                                       }
                    } else {

                        Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                    }
                    hideProgress();
                    jobCallbackListener.onFetchComplete();
                } catch (JSONException e) {

                    e.printStackTrace();
                } catch (IOException e) {

                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Toast.makeText(activity, "Internet Connection is not stable" + "\n" +t.getMessage(),Toast.LENGTH_LONG).show();
                hideProgress();
                jobCallbackListener.onFetchComplete();
            }
        });
    }


    private static void hideProgress() {

        if (Constants.progressDialog != null && Constants.progressDialog.isShowing())
            Constants.progressDialog.dismiss();
    }

}
