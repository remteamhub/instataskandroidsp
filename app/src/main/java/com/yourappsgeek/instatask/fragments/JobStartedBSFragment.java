package com.yourappsgeek.instatask.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.yourappsgeek.instatask.R;

/**
 * @author Furqan Ullah
 * email : furqan.ullah@synavos.com
 * Created on 12/10/2018.
 */

public class JobStartedBSFragment extends BottomSheetDialogFragment
{
    public JobStartedBSFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.job_started_bottom_fragment, container, false);
        v.findViewById(R.id.confirmBtn).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showBottomSheetDialogFragment();
            }
        });
        return v;
    }

    public void showBottomSheetDialogFragment()
    {
        DeliverServiceBSFragment bottomSheetFragment = new DeliverServiceBSFragment();
        bottomSheetFragment.show(getFragmentManager(), bottomSheetFragment.getTag());
        this.dismiss();

    }
    // @Override
    // public void setCancelable(boolean cancelable)
    // {
    //     // final Dialog dialog = getDialog();
    //     // View touchOutsideView = dialog.getWindow().getDecorView().findViewById(android.support.design.R.id.touch_outside);
    //     // View bottomSheetView = dialog.getWindow().getDecorView().findViewById(android.support.design.R.id.design_bottom_sheet);
    //     //
    //     // if (cancelable) {
    //     //     touchOutsideView.setOnClickListener(new View.OnClickListener() {
    //     //         @Override
    //     //         public void onClick(View v) {
    //     //             if (dialog.isShowing()) {
    //     //                 dialog.cancel();
    //     //             }
    //     //         }
    //     //     });
    //     //     BottomSheetBehavior.from(bottomSheetView).setHideable(true);
    //     // } else {
    //     //     touchOutsideView.setOnClickListener(null);
    //     //     BottomSheetBehavior.from(bottomSheetView).setHideable(false);
    //     // }
    // }


}