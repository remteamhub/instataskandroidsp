package com.yourappsgeek.instatask.fragments;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yourappsgeek.instatask.Controller.ScheduleRequest;
import com.yourappsgeek.instatask.Controller.updateStatuscController;
import com.yourappsgeek.instatask.Interfaces.ScheduleJobCallbackListener;
import com.yourappsgeek.instatask.Interfaces.buttoncallback_listener;
import com.yourappsgeek.instatask.R;
import com.yourappsgeek.instatask.Services.Constants;
import com.yourappsgeek.instatask.adapters.HistoryRequestAdapter;
import com.yourappsgeek.instatask.models.HistoryItem;
import com.yourappsgeek.instatask.models.UpdateStatus;
import com.yourappsgeek.instatask.utils.UiUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.yourappsgeek.instatask.Controller.internetCheck.isConnected;

public class RequestHistoryFragment extends Fragment implements ScheduleJobCallbackListener
{
    private List<HistoryItem> historyItems;
    RecyclerView recyclerView;
    HistoryRequestAdapter adapter;
    ScheduleRequest scheduleRequest;
    updateStatuscController controller;
    SharedPreferences preferences;
    ImageView nodata;
    ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_request_history, container, false);
        nodata=v.findViewById(R.id.nodata1);
        progressDialog = UiUtils.getProgressDialog(getContext());
        if(!isConnected(Objects.requireNonNull(getContext())));
        else {
            scheduleRequest = new ScheduleRequest(getContext(),getActivity(), this, getActivity());
            controller = new updateStatuscController(getActivity(), this);
            ConfigInitView(v);
            scheduleRequest.JobsHistory();
        }
        return v;
    }

    private void ConfigInitView(View view){
        historyItems=new ArrayList<>();

            progressDialog.show();

        preferences= PreferenceManager.getDefaultSharedPreferences(getContext());
        recyclerView = view.findViewById(R.id.historyItemView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());




            adapter = new HistoryRequestAdapter(historyItems, getContext(), R.layout.scheduled_list_item, new buttoncallback_listener() {
                @Override
                public void onClick(View view, int possition) {

                }
            });
            recyclerView.setAdapter(adapter);



    }


    @Override
    public void onFetchProgress(HistoryItem historyItem) {
            adapter.addItem(historyItem);
     //   Log.e("history", String.valueOf(historyItems.size()));



    }

    @Override
    public void onUpdateStatusProgress(UpdateStatus status) {
    }

    @Override
    public void onFetchComplete() {
            hideProgress();
        if (historyItems.size()==0){
            nodata.setVisibility(View.VISIBLE);
        }else {
            nodata.setVisibility(View.GONE);
        }

        //  Log.e("history", String.valueOf(historyItems.size()));

    }


    private  void hideProgress() {

        if (progressDialog != null && progressDialog.isShowing())
           progressDialog.dismiss();
    }
}
