package com.yourappsgeek.instatask.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.yourappsgeek.instatask.R;

/**
 * @author Furqan Ullah
 * email : furqan.ullah@synavos.com
 * Created on 12/10/2018.
 */

public class SelectServiceBSFragment extends BottomSheetDialogFragment
{

    public SelectServiceBSFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.home_bottom_fragment, container, false);
        v.findViewById(R.id.mainBtn).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showBottomSheetDialogFragment();
            }
        });
        return v;
    }

    public void showBottomSheetDialogFragment()
    {
        ConfirmServiceBSFragment bottomSheetFragment = new ConfirmServiceBSFragment();
        bottomSheetFragment.show(getFragmentManager(), bottomSheetFragment.getTag());
        this.dismiss();
    }


}