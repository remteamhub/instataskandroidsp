package com.yourappsgeek.instatask.Notification;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;


import com.yourappsgeek.instatask.R;
import com.yourappsgeek.instatask.activities.MainActivity;

import static android.content.Context.NOTIFICATION_SERVICE;

public class PushNotification {

    private Activity activity;

    public PushNotification(Activity activity) {
        this.activity = activity;
    }

    public static void showNotification(Context activity, String title, String body) {
        // intent triggered, you can add other intent for other actions
        Intent i = new Intent(activity, MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(activity, 0, i, 0);

        //Notification sound
        try {
            MediaPlayer mp;
            mp = MediaPlayer.create(activity, R.raw.name);
            mp.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // this is it, we'll build the notification!
        // in the addAction method, if you don't want any icon, just set the first param to 0
        Notification mNotification = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {

            mNotification = new Notification.Builder(activity)

                    .setContentTitle(title)
                    .setContentText(body)
                    .setSmallIcon(R.mipmap.ic_launcher_round)
                    .setContentIntent(pIntent)
                    .setAutoCancel(true)
                    .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                    .addAction(R.mipmap.ic_launcher_round, "Goto App", pIntent)
                    .build();

        }

        NotificationManager notificationManager = (NotificationManager) activity.getSystemService(NOTIFICATION_SERVICE);

        // If you want to hide the notification after it was selected, do the code below
        // myNotification.flags |= Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(0, mNotification);

    }

}

