package com.yourappsgeek.instatask.RetrofitClient;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    //    public static final String BASE_URL= "http://mce-foody.tk/admin/";
//    public static final String BASE_URL = "http://instatask.trigoncab.com/api/provider/";
    public static final String BASE_URL = "http://trigoncab.com/instatask/api/";

    public static final String IMAGE_BASE_URL="http://trigoncab.com/instatask/storage/app/";

    private static Retrofit retrofit = null;

    public static Retrofit getClient() {

        if (retrofit == null) {

            Gson gson = new GsonBuilder().setLenient().create();
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(
                    GsonConverterFactory.create(gson)).build();

        }
        return retrofit;

    }


}
