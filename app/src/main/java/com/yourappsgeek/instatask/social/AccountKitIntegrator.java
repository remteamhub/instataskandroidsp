package com.yourappsgeek.instatask.social;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;


/**
 * @author Naveed Chaudhary
 * email : naveedchaudhary300@gmail.com
 * Created on 4/4/2018.
 */

public class AccountKitIntegrator
{
    private int APP_REQUEST_CODE = 99;

    private Activity activity;
    private SocialCallback callback;
    public PhoneNumber phoneNumber;

    public AccountKitIntegrator(Activity activity, SocialCallback callback)
    {
        this.activity = activity;
        this.callback = callback;

    }


    public void phoneLogin()
    {
        final Intent intent = new Intent(activity, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN);
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        activity.startActivityForResult(intent, APP_REQUEST_CODE);
        Log.e("phonenumber", "6");

    }


    public void onActivityResult(
            final int requestCode,
            final Intent data)
    {
        if (requestCode == APP_REQUEST_CODE)
        { // confirm that this response matches your request
            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            if (loginResult.getError() != null)
            {
                Log.e("phonenumber", "1");
                callback.onSocialLoginFailed(loginResult.getError().getUserFacingMessage());
            } else if (loginResult.wasCancelled())
            {
                callback.onSocialLoginFailed("Cancelled");
                Log.e("phonenumber", "2");


            } else
            {
                if (loginResult.getAccessToken() != null)
                {
                    getAccountKitData();
                    Log.e("phonenumber", "3");

                } else
                {
                    getAccountKitData();
                    Log.e("phonenumber", "4");

                    // callback.onSocialLoginSuccess("",loginResult.getAuthorizationCode());
                }
            }
        }
    }

    private void getAccountKitData()
    {
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>()
        {
            @Override
            public void onSuccess(final Account account)
            {
                // Get phone number
                String phoneNumber = account.getPhoneNumber().toString();
             //   String email=account.getEmail().toString();
                Log.e("phonenumber", phoneNumber);



                String token;
                try
                {
                    token = AccountKit.getCurrentAccessToken().getToken();
                } catch (NullPointerException e)
                {
                    token = "";
                }
                callback.onSocialLoginSuccess(null,null,null,null,null,null,phoneNumber, token, SocialCallback.PHONE);
            }

            @Override
            public void onError(final AccountKitError error)
            {
                // Handle Error
                Log.e("phonenumber", "7");

                callback.onSocialLoginFailed(error.getUserFacingMessage());
                // Toast.makeText(SignUpActivity.this, error.getUserFacingMessage(),
                //         Toast.LENGTH_SHORT).show();
            }
        });
    }
}
