package com.yourappsgeek.instatask;

import android.util.Log;

import com.yourappsgeek.instatask.config.NetworkConfig;
import com.yourappsgeek.instatask.config.WebService;
import com.yourappsgeek.instatask.network.NetworkResponseCallback;
import com.yourappsgeek.instatask.network.OkHttpHandler;
import com.yourappsgeek.instatask.network.ResponseCallback;
import com.yourappsgeek.instatask.parser.JsonParserImp;
import com.yourappsgeek.instatask.requests.RegisterRequest;
import com.yourappsgeek.instatask.requests.SignInRequest;
import com.yourappsgeek.instatask.response.RegisterResponse;
import com.yourappsgeek.instatask.response.SignInResponse;
import com.yourappsgeek.instatask.threading.MainThreadImpl;

/**
 * @author Furqan Ullah
 * email : furqan.ullah@synavos.com
 * Created on 12/20/2018.
 */
public class UserRespository {

    public void signIn(SignInRequest request, final NetworkResponseCallback<SignInResponse> callback) {
        String url = NetworkConfig.SERVER_URL + WebService.LOGIN_CUSTOMER;

        Log.e("serverurl", String.valueOf(url));
        Log.e("serverurl", String.valueOf(request));
        OkHttpHandler.getOurInstance().sendPostRequest(url, request, new ResponseCallback() {
            @Override
            public void onSuccess(final String response) {
                SignInResponse response1 = JsonParserImp.getOurInstance().parserSignIn(response);
                callback.onSuccess(response1);

            }

            @Override
            public void onFailed(final String msg) {
                callback.onFailure(922, msg);

            }
        }, MainThreadImpl.getInstance());

    }

    public void signUp(RegisterRequest request, final NetworkResponseCallback<RegisterResponse> callback) {
        String url = "http://instatask.trigoncab.com/api/provider/register?";

        OkHttpHandler.getOurInstance().sendPostRequest(url, request, new ResponseCallback() {
            @Override
            public void onSuccess(final String response) {
                RegisterResponse response1 = JsonParserImp.getOurInstance().parserRegister(response);
                callback.onSuccess(response1);
            }

            @Override
            public void onFailed(final String msg) {
                callback.onFailure(922, msg);
            }
        }, MainThreadImpl.getInstance());

    }
}
