package com.yourappsgeek.instatask.Interfaces;

import com.yourappsgeek.instatask.models.HistoryItem;
import com.yourappsgeek.instatask.models.UpdateStatus;

public interface ScheduleJobCallbackListener {

    void onFetchProgress(HistoryItem historyItem);
    void onUpdateStatusProgress(UpdateStatus status);
    void onFetchComplete();
}
