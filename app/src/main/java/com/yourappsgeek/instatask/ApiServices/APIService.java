package com.yourappsgeek.instatask.ApiServices;

import android.net.Uri;

import com.yourappsgeek.instatask.PojostatusUpdate.StatusUpdateResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface APIService {

    //    @FormUrlEncoded
//    user_login_api.php
    @POST("provider/login")
    Call<ResponseBody> userSignIn(
            @Query("email") String email,
            @Query("password") String password,
            @Query("fcm_token") String token);


    @POST("provider/logout")
    Call<ResponseBody> userLogout(
            @Query("fcm_token") String token);

    @POST("jobs/leftJob")
    Call<ResponseBody> cancelJob(
            @Query("job_id") String jobId);


    @POST("provider/register")
    Call<ResponseBody> createUser(
            @Query("username") String fullname,
            @Query("first_name") String first_name,
            @Query("last_name") String last_name,
            @Query("email") String email,
            @Query("phone") String phone,
            @Query("radius") int radius,
            @Query("password") String password,
            @Query("fcm_token") String fcm_token);


//    @Multipart
//    @POST("provider/socialLogin")
//    Call<ResponseBody> SocialLogin(
//            @Part("social_id") RequestBody social_id,
//            @Part("username") RequestBody username,
//            @Part("fcm_token") RequestBody fcm_token,
//            @Part("email") RequestBody email,
//            @Part("first_name") RequestBody first_name,
//            @Part("last_name") RequestBody last_name,
//            @Part("avatar_file") RequestBody avatar_file,
//            @Part("lat") double lat,
//            @Part("lng") double lng,
//            @Part("radius") RequestBody radius,
//            @Part MultipartBody.Part image);

    @POST("provider/socialLogin")
    Call<ResponseBody> SocialLogin(
            @Query("social_id") String social_id,
            @Query("username") String username,
            @Query("fcm_token") String fcm_token,
            @Query("email") String email,
            @Query("first_name") String first_name,
            @Query("last_name") String last_name,
            @Query("avatar_file") String avatar_file,
            @Query("lat") double lat,
            @Query("lng") double lng,
            @Query("radius") String radius);


    @POST("provider/updateWorkStatus")
    Call<ResponseBody> workStatusUpdate(
            @Query("job_status") int statusvalue,
            @Query("user_id") int userId,
            @Query("fcm_token") String token);


    @Multipart
    @POST("provider/updateProfile")
    Call<ResponseBody> userProfileUpdateLicense(
            @Part("user_id") int id,
            @Part("fcm_token") RequestBody token,
            @Part MultipartBody.Part image);


    @POST("provider/updateLatLng")
    Call<ResponseBody> userProfileUpdateLatiLogi(
            @Query("user_id") int id,
            @Query("fcm_token") String token,
            @Query("lat") Double lat,
            @Query("lng") Double log);

    @POST("provider/updatePassword")
    Call<ResponseBody> updatePassword(
            @Query("user_id") int id,
            @Query("fcm_token") String token,
            @Query("old_password") String oldP,
            @Query("new_password") String newP);

    @POST("provider/updateProfile")
    Call<ResponseBody> updateProfile(
            @Query("user_id") int id,
            @Query("fcm_token") String token,
            @Query("radius") int radius,
            @Query("phone") String phone
    );



    @Multipart
    @POST("provider/updateProfile")
    Call<ResponseBody> ImageUpdate(
            @Part("user_id") int id,
            @Part("fcm_token") RequestBody token,
            @Part("radius") int radius,
            @Part("first_name") RequestBody first_name,
            @Part("last_name") RequestBody last_name,
            @Part MultipartBody.Part image);

    @POST("jobs/checkCurrentJob")
    Call<ResponseBody> checkCurrentJob(
            @Query("user_id") String userId,
            @Query("type") String type);

    @POST("jobs/checkJobById")
    Call<ResponseBody> checkJobById(@Query("job_id") String jobId);

    @POST("jobs/acceptJob")
    Call<ResponseBody> acceptjob(
            @Query("job_id") String job_id,
            @Query("driver_id") int driver_id);

    @POST("provider/rating")
    Call<ResponseBody> rating(
            @Query("job_id") int job_id,
            @Query("customer_rating") float customer_rating);


    @Multipart
    @POST("jobs/updateStatus")
    Call<ResponseBody> updateStatus(
            @Part("job_status") int job_status,
            @Part("job_id") int job_id,
            @Part MultipartBody.Part current_situation_img);

    @Multipart
    @POST("provider/requestJobApproval")
    Call<ResponseBody> requestJobApproval(
            @Part("job_id") int job_id,
            @Part("driver_id") int driver_id,
            @Part MultipartBody.Part after_work_img);

    @GET("general/limitations")
    Call<ResponseBody> limitations();

    @POST("jobs/checkPendingJob")
    Call<ResponseBody> checkPendingJob(
            @Query("user_id") String userId,
            @Query("type") String type);


    @POST("jobs/checkPreviousJob")
    Call<ResponseBody> checkPreviousJob(
            @Query("user_id") String userId,
            @Query("type") String type);

    @POST("jobs/editRequestStatus")
    Call<ResponseBody> editRequestStatus(
            @Query("job_id") int job_id,
            @Query("status") int status);

    @POST("jobs/rejectJob")
    Call<ResponseBody> rejectJob(
            @Query("provider_id") int provider_id,
            @Query("job_id") int job_id);

    @POST("insertIssue")
    Call<ResponseBody> help(
            @Query("user_id") int provider_id,
            @Query("user_type") String user_type,
            @Query("issue_name") String issue_name,
            @Query("issue_type") String issue_type,
            @Query("issue_description") String issue_description,
            @Query("status") String status);

    @POST("chat/send")
    Call<ResponseBody> sendMessage(
            @Query("to") String to,
            @Query("from") String from,
            @Query("message") String message);

    @POST("chat/get")
    Call<ResponseBody> getMessage(
            @Query("user1") String to,
            @Query("user2") String from,
            @Query("page") int page);

    @POST("provider/bankinfo")
    Call<ResponseBody> bankinfo(
            @Query("user_id") String user_id,
            @Query("bank_name") String bank_name,
            @Query("swift_code") String swift_code,
            @Query("IBAN") String IBAN,
            @Query("account_no") String account_no,
            @Query("country") String country,
            @Query("withdraw_amount") String withdraw_amount);

    @POST("provider/forgetpassword")
    Call<ResponseBody> forgetpassword(
            @Query("email") String email
           );

    @POST("provider/getforgetcode")
    Call<ResponseBody> getforgetcode(
            @Query("email") String email,
            @Query("code") String code

    );

    @GET("provider/bankinfo")
    Call<ResponseBody> getbankinfo(
            @Query("user_id") String user_id
    );

    @POST("provider/updatePassword")
    Call<ResponseBody> ChangePassword(
            @Query("user_id") int id,
            @Query("fcm_token") String token,
            @Query("no_old") String oldP,
            @Query("new_password") String newP);
}
