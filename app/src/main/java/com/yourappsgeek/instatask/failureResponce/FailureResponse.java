package com.yourappsgeek.instatask.failureResponce;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class FailureResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("error_msg")
	private String errorMsg;

	@SerializedName("data")
	private List<Object> data;

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setErrorMsg(String errorMsg){
		this.errorMsg = errorMsg;
	}

	public String getErrorMsg(){
		return errorMsg;
	}

	public void setData(List<Object> data){
		this.data = data;
	}

	public List<Object> getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"FailureResponse{" + 
			"code = '" + code + '\'' + 
			",error_msg = '" + errorMsg + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}