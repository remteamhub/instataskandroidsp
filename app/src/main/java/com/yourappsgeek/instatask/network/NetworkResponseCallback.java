package com.yourappsgeek.instatask.network;

/**
 * @author Furqan Ullah
 * email :  furqanullah717@gmail.com
 * Created on 11/14/2018.
 */
public interface NetworkResponseCallback<C>
{
    void onSuccess(C response);

    void onFailure(int errorCode, String errorMessage);
}
