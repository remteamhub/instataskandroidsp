package com.yourappsgeek.instatask.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterRequest extends WebServiceRequest {


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public String getAvatar_file() {
        return avatar_file;
    }

    public void setAvatar_file(String avatar_file) {
        this.avatar_file = avatar_file;
    }

    public String getLicense_img() {
        return license_img;
    }

    public void setLicense_img(String license_img) {
        this.license_img = license_img;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("first_name")
    @Expose
    private String first_name;
    @SerializedName("last_name")
    @Expose
    private String last_name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("radius")
    @Expose
    private String radius;
    @SerializedName("avatar_file")
    @Expose
    private String avatar_file;
    @SerializedName("license_img")
    @Expose
    private String license_img;
    @SerializedName("password")
    @Expose
    private String password;

    public String getTwAccessToken() {
        return TwAccessToken;
    }

    public void setTwAccessToken(String twAccessToken) {
        TwAccessToken = twAccessToken;
    }

    public String getTwAccessTokenSecret() {
        return TwAccessTokenSecret;
    }

    public void setTwAccessTokenSecret(String twAccessTokenSecret) {
        TwAccessTokenSecret = twAccessTokenSecret;
    }

    @SerializedName("TwAccessToken")
    @Expose
    private String TwAccessToken;
    @SerializedName("TwAccessTokenSecret")
    @Expose
    private String TwAccessTokenSecret;

    public String getFbAccessToken() {
        return FbAccessToken;
    }

    public void setFbAccessToken(String fbAccessToken) {
        FbAccessToken = fbAccessToken;
    }

    @SerializedName("FbAccessToken")
    @Expose
    private String FbAccessToken;

}