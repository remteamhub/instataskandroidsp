package com.yourappsgeek.instatask.requests;


import com.google.gson.annotations.SerializedName;
import com.yourappsgeek.instatask.model.WebServiceHeader;

import java.io.Serializable;
import java.util.HashMap;

/**
 * @author Furqan Khan
 * Author Email: furqanullah717@gmail.com
 * Created on: 16/02/2018
 */

public abstract class WebServiceRequest implements Serializable
{
    /**
     * This object will contain all the fields that are meant to add in http header
     */
    private transient WebServiceHeader header;

    @SerializedName("channel")
    private String channel;

    // ========== Constructor ========== //

    public WebServiceRequest()
    {
        this.header = new WebServiceHeader();
    }

    // ========== Getters & Setters ========== //

    public WebServiceHeader getHeader()
    {
        if (header == null)
            header = new WebServiceHeader();
        return header;
    }

    public void setHeader(WebServiceHeader header)
    {
        this.header = header;
    }

    public HashMap<String, String> getHeaderData()
    {
        return getHeader().getHeaderMap();
    }

    public String getChannel()
    {
        return channel;
    }

    public void setChannel(String channel)
    {
        this.channel = channel;
    }
}
