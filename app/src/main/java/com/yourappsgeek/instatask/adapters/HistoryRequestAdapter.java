package com.yourappsgeek.instatask.adapters;

import android.content.Context;
import android.content.Intent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.yourappsgeek.instatask.Interfaces.buttoncallback_listener;
import com.yourappsgeek.instatask.R;
import com.yourappsgeek.instatask.activities.DetailsHistoryRequest;
import com.yourappsgeek.instatask.models.HistoryItem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * @author Naveed Chaudhary
 * email : naveedchaudhary300@gmail.com
 * Created on 07/20/2019.
 */
public class HistoryRequestAdapter extends RecyclerView.Adapter<HistoryRequestAdapter.ScheduleHolder> {
    buttoncallback_listener buttoncallback_listener;
    List<HistoryItem> list;
    Context context;

    int layout;

    public HistoryRequestAdapter(List<HistoryItem> list, Context context, int layout, buttoncallback_listener listener) {
        this.context = context;
        this.list = list;
        this.layout = layout;
        this.buttoncallback_listener = listener;
    }

    public void addItem(HistoryItem schedule) {
        list.add(schedule);
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ScheduleHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int possition) {
        View row = LayoutInflater.from(viewGroup.getContext()).inflate(layout, viewGroup, false);
        return new ScheduleHolder(row);
    }

    @Override
    public void onBindViewHolder(@NonNull ScheduleHolder scheduleHolder, final int possition) {
        final HistoryItem item = list.get(possition);
        scheduleHolder.address.setText(item.address);
        scheduleHolder.service_price.setText("$" + item.price);




        try {
            long unixdate = Long.parseLong(item.date_time);
        java.util.Date d = new java.util.Date(unixdate*1000L);
        String itemDateStr = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa", Locale.US).format(d);
            scheduleHolder.time_date.setText(itemDateStr);
        }catch (Exception e){
            e.printStackTrace();
        }



        scheduleHolder.customer_name.setText(item.customer_name);
        scheduleHolder.service_type.setText(item.service_type);
        scheduleHolder.materialRatingBar.setRating(Float.parseFloat(item.rating));
        if (scheduleHolder.mapView != null) {
            // Initialise the MapView
            scheduleHolder.mapView.onCreate(null);
            // Set the map ready callback to receive the GoogleMap object
            scheduleHolder.mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                    // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
                    try {
                        MapsInitializer.initialize(context);
                        LatLng sydney = new LatLng(list.get(possition).latitude, list.get(possition).longitude);
                        googleMap.addMarker(new MarkerOptions().position(sydney).title("My Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_marker)));
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 15.0f));
                        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                            @Override
                            public void onMapClick(LatLng latLng) {
                                //do nothing, we want to suppress launching Google Maps

                                Intent intent = new Intent(context, DetailsHistoryRequest.class);
                                intent.putExtra("lat", list.get(possition).latitude);
                                intent.putExtra("long", list.get(possition).longitude);
                                intent.putExtra("price", list.get(possition).price);
                                intent.putExtra("lane", list.get(possition).service_type);
                                intent.putExtra("provideName", list.get(possition).customer_name);
                                intent.putExtra("rating", list.get(possition).rating);
                                intent.putExtra("before_img", list.get(possition).before_img);
                                intent.putExtra("after_img", list.get(possition).after_img);
                                context.startActivity(intent);
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        scheduleHolder.itemView.setOnClickListener(view -> {
//                list.get(possition).address;

            Intent intent = new Intent(context, DetailsHistoryRequest.class);
            intent.putExtra("lat", list.get(possition).latitude);
            intent.putExtra("long", list.get(possition).longitude);
            intent.putExtra("price", list.get(possition).price);
            intent.putExtra("lane", list.get(possition).service_type);
            intent.putExtra("provideName", list.get(possition).customer_name);
            intent.putExtra("rating", list.get(possition).rating);
            intent.putExtra("before_img", list.get(possition).before_img);
            intent.putExtra("after_img", list.get(possition).after_img);
            context.startActivity(intent);
        });


    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ScheduleHolder extends RecyclerView.ViewHolder {


        TextView address, service_price, time_date, customer_name, service_type;
        MaterialRatingBar materialRatingBar;
        MapView mapView;
        Button button;

        public ScheduleHolder(@NonNull View itemView) {
            super(itemView);
            address = itemView.findViewById(R.id.textView12);
            service_price = itemView.findViewById(R.id.textView14);
            time_date = itemView.findViewById(R.id.textView13);
            customer_name = itemView.findViewById(R.id.textView40);
            service_type = itemView.findViewById(R.id.textView42);
            button = itemView.findViewById(R.id.button5);
            mapView = itemView.findViewById(R.id.mapView2);
            materialRatingBar = itemView.findViewById(R.id.ratingBar);
            button.setOnClickListener(view -> buttoncallback_listener.onClick(view, getAdapterPosition()));

        }


    }
}
