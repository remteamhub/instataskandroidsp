package com.yourappsgeek.instatask.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.yourappsgeek.instatask.models.Customer;

/**
 * @author Furqan Ullah
 * email : furqan.ullah@synavos.com
 * Created on 12/20/2018.
 */
public class RegisterResponse extends WebServiceResponse
{
    @SerializedName("_message")
    @Expose
    private String message;
    @SerializedName("customer")
    @Expose
    private Customer customer;

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public Customer getCustomer()
    {
        return customer;
    }

    public void setCustomer(Customer customer)
    {
        this.customer = customer;
    }

}
