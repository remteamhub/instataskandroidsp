package com.yourappsgeek.instatask.parser;


import com.google.gson.Gson;
import com.yourappsgeek.instatask.response.RegisterResponse;
import com.yourappsgeek.instatask.response.SignInResponse;

/**
 * @author Furqan Ullah
 * email :  furqanullah717@gmail.com
 * Created on 11/15/2018.
 */
public class JsonParserImp implements JsonParser
{

    private Gson getGsonParser()
    {
        return new Gson();
    }

    @Override
    public SignInResponse parserSignIn(String data)
    {
        return getGsonParser().fromJson(data,SignInResponse.class);
    }


    private static class SingletonHolder
    {
        private static final JsonParserImp INSTANCE = new JsonParserImp();
    }

    public static JsonParserImp getOurInstance()
    {
        return SingletonHolder.INSTANCE;
    }

    private JsonParserImp()
    {
    }

    @Override
    public RegisterResponse parserRegister(String data)
    {
        return getGsonParser().fromJson(data,RegisterResponse.class);
    }



}
