package com.yourappsgeek.instatask.parser;

import com.yourappsgeek.instatask.response.RegisterResponse;
import com.yourappsgeek.instatask.response.SignInResponse;

/**
 * @author Furqan Ullah
 * email :  furqanullah717@gmail.com
 * Created on 11/15/2018.
 */
public interface JsonParser
{
    SignInResponse parserSignIn(String data);
     RegisterResponse parserRegister(String data);

}
