package com.yourappsgeek.instatask.executor;

/**
 * This executor is responsible for running interactors on background thread
 *
 * @author Furqan Khan
 *  ™
 * Author Email: furqanullah717@gmail.com
 * Created on: 16/02/2018
 */

public interface Executor
{
    // /**
    //  * This method should call the interactor's run method and thus start the interactor. This should be
    //  * called on a background thread as interactors might do lengthy operations
    //  *
    //  * @param interactor The interactor to run
    //  */
    // void execute(final BaseInteractor interactor);
}
