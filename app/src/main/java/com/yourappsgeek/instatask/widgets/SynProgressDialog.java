package com.yourappsgeek.instatask.widgets;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;

import java.lang.ref.WeakReference;


public class SynProgressDialog extends ProgressDialog
{
    private WeakReference<Activity> activityReference;

    public SynProgressDialog(Context context)
    {
        super(context);
        this.activityReference = new WeakReference<>((Activity) context);
    }

    public SynProgressDialog(Context context, int theme)
    {
        super(context, theme);
        this.activityReference = new WeakReference<>((Activity) context);
    }

    @Override
    public void dismiss()
    {
        // Only dismiss dialog after making sure that dialog has a valid reference of the activity
        if (this.isShowing() &&
                activityReference != null &&
                activityReference.get() != null &&
                !activityReference.get().isFinishing())
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 &&
                    activityReference.get().isDestroyed())
                return;

            try
            {
                super.dismiss();
            } catch (IllegalStateException e)
            {
            }
        }
    }
}
