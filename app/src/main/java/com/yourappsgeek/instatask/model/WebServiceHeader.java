package com.yourappsgeek.instatask.model;



import com.yourappsgeek.instatask.utils.HeaderParams;

import java.io.Serializable;
import java.util.HashMap;

/**
 * @author Furqan Khan
 *
 * Author Email: furqanullah717@gmail.com
 * Created on: 16/02/2018
 */

public class WebServiceHeader implements Serializable
{
    private HashMap<String, String> headerMap;

    // ========== Class Methods ========== //

    public WebServiceHeader()
    {
        this.headerMap = new HashMap<>();
    }

    public HashMap<String, String> getHeaderMap()
    {
        return headerMap;
    }

    // ========== Utility Getters & Setters ========== //

    public String getSessionId()
    {
        return this.headerMap.get(HeaderParams.AUTH_TOKEN);
    }

    public void setSessionId(String sessionId)
    {
        this.headerMap.put(HeaderParams.AUTH_TOKEN, sessionId);
    }

    public String getContentType()
    {
        return this.headerMap.get(HeaderParams.CONTENT_TYPE);
    }

    public void setContentType(String getContentType)
    {
        this.headerMap.put(HeaderParams.CONTENT_TYPE, getContentType);
    }

    public String getUserAgent()
    {
        return this.headerMap.get(HeaderParams.USER_AGENT);
    }

    public void setUserAgent(String userAgent)
    {
        this.headerMap.put(HeaderParams.USER_AGENT, userAgent);
    }
}
