package com.yourappsgeek.instatask.model;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

/**
 * @author Furqan Khan
 *
 * Author Email: furqanullah717@gmail.com
 * Created on: 16/02/2018
 */

public abstract class BaseModel
{
    @Override
    public String toString()
    {
        Class classObj = this.getClass();
        List<Field> fields = Arrays.asList(classObj.getDeclaredFields());
        List<Field> extraFields = Arrays.asList(classObj.getFields());

        StringBuilder result = new StringBuilder();
        boolean isFirst = true;

        result.append(classObj.getSimpleName());
        result.append(" (");

        for (Field field : fields)
        {
            if (extraFields.contains(field))
                continue;

            if (!isFirst)
            {
                result.append(", ");
            }

            try
            {
                field.setAccessible(true);
                result.append(field.getName());
                result.append(": ");
                result.append(field.get(this));
            } catch (IllegalAccessException ex)
            {
                // new Log(this).e(ex);
            }
            isFirst = false;
        }
        result.append(")");

        return result.toString();
    }
}
