package com.yourappsgeek.instatask.SharedPreference;

public class PreferencesUtility {

    // Values for Shared Prefrences
    public static final String LOGGED_IN_PREF = "logged_in_status";

    public static final String LICENSE_IMAGE = "license_image";

    public static final String FCM_TOKEN="fcm_token";

    public static final String PROFILE_IMAGE="profile_image";

}
