package com.yourappsgeek.instatask.SharedPreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import static com.yourappsgeek.instatask.SharedPreference.PreferencesUtility.FCM_TOKEN;
import static com.yourappsgeek.instatask.SharedPreference.PreferencesUtility.LICENSE_IMAGE;
import static com.yourappsgeek.instatask.SharedPreference.PreferencesUtility.LOGGED_IN_PREF;
import static com.yourappsgeek.instatask.SharedPreference.PreferencesUtility.PROFILE_IMAGE;

public class SaveSharedPreference {


    static SharedPreferences getPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * Set the Login Status
     *
     * @param context
     * @param loggedIn
     */
    public static void setLoggedIn(Context context, boolean loggedIn) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putBoolean(LOGGED_IN_PREF, loggedIn);
        editor.apply();
    }

    public static void setFCM_Token(Context context, String fcm_token) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(FCM_TOKEN, fcm_token);
        editor.apply();
    }


    public static void setProfile_Image(Context context, String pro_img) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString(PROFILE_IMAGE, pro_img);
        editor.apply();
    }

    /**
     * Get the Login Status
     *
     * @param context
     * @return boolean: login status
     */
    public static boolean getLoggedStatus(Context context) {
        return getPreferences(context).getBoolean(LOGGED_IN_PREF, false);
    }

    public static String getFCM_Token(Context context) {
        return getPreferences(context).getString(FCM_TOKEN, "");
    }

    public static String getProfile_Image(Context context) {
        return getPreferences(context).getString(PROFILE_IMAGE, "");
    }
}
