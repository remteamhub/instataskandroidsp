package com.yourappsgeek.instatask.PojostatusUpdate;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("status")
	private String status;

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"status = '" + status + '\'' + 
			"}";
		}
}