package com.yourappsgeek.instatask.PojostatusUpdate;

import com.google.gson.annotations.SerializedName;

public class StatusUpdateResponse {

    @SerializedName("code")
    private int code;

    @SerializedName("error_msg")
    private String errorMsg;

    @SerializedName("data")
    private Data data;

    public void setCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Data getData() {
        return data;
    }

    @Override
    public String toString() {
        return
                "StatusUpdateResponse{" +
                        "code = '" + code + '\'' +
                        ",error_msg = '" + errorMsg + '\'' +
                        ",data = '" + data + '\'' +
                        "}";
    }
}