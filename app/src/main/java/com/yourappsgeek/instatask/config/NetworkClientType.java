package com.yourappsgeek.instatask.config;

/**
 * Lists all supported network clients
 *
 * @author Furqan Khan
 *
 * Author Email: furqanullah717@gmail.com
 * Created on: 16/02/2018
 */

public enum NetworkClientType
{
    NATIVE_ANDROID,
    OK_HTTP,
    SOAP_CLIENT
}
