package com.yourappsgeek.instatask.config;

/**
 * Contains all the supported web services
 *
 * @author Furqan Khan
 *
 * Author Email: furqanullah717@gmail.com
 * Created on: 16/02/2018
 */

public enum NetworkServiceType
{
    REST,
    SOAP
}
