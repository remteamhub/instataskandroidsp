package com.yourappsgeek.instatask.config;

/**
 * Contains network configurations
 *
 * @author Furqan Khan
 *
 * Author Email: furqanullah717@gmail.com
 * Created on: 16/02/2018
 */

public class NetworkConfig
{
    public static final NetworkServiceType NETWORK_SERVICE_TYPE = NetworkServiceType.REST;
    public static final NetworkResponseType NETWORK_RESPONSE_TYPE = NetworkResponseType.JSON;

    public static final NetworkClientType NETWORK_CLIENT = NetworkClientType.OK_HTTP;
    public static final ResponseParserType RESPONSE_PARSER = ResponseParserType.GSON;

    public static final String CONTENT_TYPE_DEFAULT = "application/json";

    // Timeouts are in seconds
    public static final int CONNECTION_TIMEOUT = 10;
    public static final int NETWORK_WRITE_TIMEOUT = 10;
    public static final int NETWORK_READ_TIMEOUT = 30;

    // === SERVER URLs === //
    private class ServerUrl
    {
//        private static final String SERVERURL = "https://instatask-node.herokuapp.com/";
        private static final String SERVERURL = "http://instatask.trigoncab.com/api/provider/";

    }
//    https://instatask-node.herokuapp.com/
    // === SERVER CONFIGS === //
    public static final String SERVER_URL = ServerUrl.SERVERURL;
    public static final String USER_AGENT_PARAM = "TrackingUser-Agent";
    public static String USER_AGENT = null;
}
