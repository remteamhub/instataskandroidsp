package com.yourappsgeek.instatask.config;

/**
 * Contains all the supported response parsers
 *
 * @author Furqan Khan
 *
 * Author Email: furqanullah717@gmail.com
 * Created on: 16/02/2018
 */

public enum ResponseParserType
{
    NATIVE_JSON,
    GSON,
    XML_PULL_PARSER,
    K_SOAP2
}
